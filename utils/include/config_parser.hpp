#pragma once

#include <sstream>
#include <string>
#include <unordered_map>

const std::string CONFIG_FILE = "../config.txt";


namespace Config {
	// identifier {DELIMS} TK_ASSIGN {DELIMS} value {DELIMS} \n

	const std::string TK_ASSIGN     = "=";
	const std::string TK_COMMENT    = "#";

	const std::string DELIMS        = " \t\n";
	
	extern std::unordered_map<std::string, std::string> confmap;
	

	void parse();

	template<typename T>
	T convert(const std::string& str) {
		std::stringstream ss;
		T result;
		ss << str;
		ss >> result;
		return result;
	}

	template<typename T>
	T get(const std::string& key) {
		auto it = confmap.find(key);
		if (it == confmap.end())
			throw std::out_of_range("could not find config entry named '" + key + "'");
		return convert<T>(it->second);
	}
}