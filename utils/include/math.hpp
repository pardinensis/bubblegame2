#pragma once

#include <limits>
#include <iostream>
#include <cmath>

const float POSITIVE_INFINITY =  std::numeric_limits<float>::infinity();
const float NEGATIVE_INFINITY = -std::numeric_limits<float>::infinity();

template <typename T>
T min(T a, T b) {
	return (a > b) ? b : a;
}

template <typename T>
T max(T a, T b) {
	return (a < b) ? b : a;
}

template <typename T>
T min(T a, T b, T c) {
	return min(a, min(b, c));
}

template <typename T>
T max(T a, T b, T c) {
	return max(a, max(b, c));
}

template <typename T>
T min(T a, T b, T c, T d) {
	return min(a, min(b, c, d));
}

template <typename T>
T max(T a, T b, T c, T d) {
	return max(a, max(b, c, d));
}

uint log2(uint i);
uint exp2(uint i);
bool is_exp2(uint i);