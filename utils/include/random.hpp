#pragma once

#include <glm/glm.hpp>

namespace Random {
	void seed(unsigned int seed);

	float get_float();
	float get_float(float min, float max);

	double get_double();
	double get_double(double min, double max);

	int get_int(int n);
	int get_int(int min, int max);

	bool get_bool(double probability);

	glm::vec3 get_unit_vector();
}