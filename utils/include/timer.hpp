#pragma once

#include <chrono>


typedef std::chrono::high_resolution_clock::time_point time_point;

class Timer {
private:
	time_point start_time;

public:
	void restart();
	double measure();
};