#pragma once

#include "timer.hpp"

class FPS {
private:
	double alpha;
	double fps;
	double time_since_last_print;
	Timer timer;

public:
	FPS(double alpha = 0.05);

	void print(double interval);
	double update();
};