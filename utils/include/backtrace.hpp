#ifndef _STACKTRACE_H_
#define _STACKTRACE_H_

#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <cxxabi.h>
#include <sstream>

std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

static inline void print_stacktrace(FILE *out = stderr, unsigned int max_frames = 63) {
    void* addrlist[max_frames+1];
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));
    if (addrlen == 0) {
		fprintf(out, "  <empty, possibly corrupt>\n");
		return;
    }

    char** symbollist = backtrace_symbols(addrlist, addrlen);
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    for (int i = 2; i < addrlen; i++) {
		char *begin_name = 0;
		for (char *p = symbollist[i]; *p; ++p)
		    if (*p == '(')
				begin_name = p;

	    char syscom[256];
		std::string executable = std::string(symbollist[i]).substr(0, begin_name - symbollist[i]);
	    sprintf(syscom,"addr2line %p -f -C -e %s", addrlist[i], executable.c_str());
	    std::string str = exec(syscom);
	    std::istringstream iss(str);
	    std::string line1, line2;
	    while (std::getline(iss, line1) && std::getline(iss, line2)) {
	    	if (line2[0] != '?') {
	    		int n_chars = 0;
	    		if (line1[0] != '?') {
    				std::string function_name = line1.substr(0, line1.find("("));
    				std::cout << function_name << "()";
    				n_chars += function_name.size();
    				do {
    					std::cout << " ";
    				} while (++n_chars < 42);
	    		}
	    		auto filename_start = line2.rfind("/");
	    		if (filename_start != std::string::npos)
	    			line2 = line2.substr(filename_start + 1);
				std::cout << line2.substr(0, line2.find("(")) << std::endl;
			}
	    }
    }

    free(funcname);
    free(symbollist);
}

#endif // _STACKTRACE_H_