#include "fps.hpp"

#include <iostream>

FPS::FPS(double alpha) : alpha(alpha), fps(60.0), time_since_last_print(0) {
	timer.restart();
}

void FPS::print(double interval) {
	update();
	if (time_since_last_print > interval) {
		std::cout << "FPS: " << fps << std::endl;
		time_since_last_print = 0;
	}
}


double FPS::update() {
	double t = timer.measure();
	timer.restart();
	time_since_last_print += t;
	fps = (1 - alpha) * fps + alpha * (1.0 / t);
	return fps;
}