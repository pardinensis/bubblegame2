#include "math.hpp"

uint log2(uint i) {
	if (i == 0) {
		std::cerr << "ERROR: log2 of 0 is not defined" << std::endl;
		return 0;
	}
	uint l = 0;
	while (i >>= 1) ++l;
	return l;
}

uint exp2(uint i) {
	return 1 << i;
}

bool is_exp2(uint i) {
	return !((i) & (i-1));
}