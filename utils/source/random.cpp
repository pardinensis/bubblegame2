#include "random.hpp"

using namespace Random;

#include <cassert>
#include <chrono>
#include <random>
#include <cmath>

#define GLM_FORCE_RADIANS
#include <glm/gtx/polar_coordinates.hpp>


std::default_random_engine engine(std::chrono::system_clock::now().time_since_epoch().count());

void Random::seed(unsigned int seed) {
	engine.seed(seed);
}

float Random::get_float() {
	return (float) get_double();
}

float Random::get_float(float min, float max) {
	return (float) get_double(min, max);
}

double Random::get_double() {
	return double(engine() - engine.min()) / double((unsigned long long)engine.max() - engine.min() + 1);
}

double Random::get_double(double min, double max) {
	return double(engine() - engine.min()) / double(engine.max() - engine.min()) * (max - min) + min;
}

int Random::get_int(int n) {
	assert(n > 0);
	return engine() % n;
}

int Random::get_int(int min, int max) {
	assert(min <= max);
	return engine() % (max - min + 1) + min;
}

bool Random::get_bool(double probability) {
	assert (probability >= 0 && probability <= 1);
	return get_double() < probability;
}

glm::vec3 Random::get_unit_vector() {
	float u = get_float();
	float v = get_float();
	float theta = 2 * M_PI * u;
	float phi = acos(2 * v - 1);
	glm::vec2 polar(theta, phi);
	return glm::euclidean(polar);
}