#include "config_parser.hpp"

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>


std::unordered_map<std::string, std::string> Config::confmap;


#define ERROR(msg, lineno) \
	throw std::runtime_error("Error in config.txt, line " + std::to_string(lineno) + ": " + msg);



namespace Config {
	template<>
	std::string convert<std::string>(const std::string& str) {
		return str;
	}
}

void Config::parse() {
	std::fstream fs;
	fs.open(CONFIG_FILE, std::fstream::in);
	if (!fs.is_open()) {
		ERROR("could not open config.txt", -1)
	}

	std::string line;
	std::string str_identifier, str_value;
	int line_number = 0;
	while (std::getline(fs, line)) {
		++line_number;

		// remove comments
		size_t comment_start = line.find(TK_COMMENT);
		if (comment_start != line.npos) {
			line = line.substr(0, comment_start);
		}

		// ignore empty lines
		size_t first_char = line.find_first_not_of(DELIMS);
		if (first_char == std::string::npos) {
			continue;
		}

		// remove leading whitespaces
		line = line.substr(first_char);
		
		// find the assignment token
		size_t pos_assign = line.find(TK_ASSIGN);
		if (pos_assign == std::string::npos)
			ERROR("no assignment token found", line_number);

		// extract the identifier
		if (pos_assign == 0)
			ERROR("no identifier found", line_number);
		size_t pos_identifier_end = line.find_last_of(DELIMS, pos_assign - 1);
		str_identifier = line.substr(0, pos_identifier_end);

		// extract the value
		size_t pos_value = line.find_first_not_of(DELIMS, pos_assign + TK_ASSIGN.size());
		size_t pos_value_end = line.find_last_not_of(DELIMS) + 1;
		str_value = line.substr(pos_value, pos_value_end - pos_value);

		confmap[str_identifier] = str_value;
	}
}
