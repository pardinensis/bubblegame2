#include "timer.hpp"

void Timer::restart() {
	start_time = std::chrono::high_resolution_clock::now();
}

double Timer::measure() {
	time_point end_time = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_diff = end_time - start_time;
	return time_diff.count();
}