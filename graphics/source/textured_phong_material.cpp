#include "textured_phong_material.hpp"

#include "shader.hpp"

TexturedPhongMaterial::TexturedPhongMaterial(const std::string& tex_file) : texture(tex_file) {
	static bool initialized = false;
	if (!initialized) {
		shader::create("texturedphongmaterial", "pos_norm_tc.vs.glsl", GL_VERTEX_SHADER);
		shader::create("texturedphongmaterial", "ds_geometry_textured.fs.glsl", GL_FRAGMENT_SHADER);
		initialized = true;
	}

	texture.set_mipmapping(true);
	texture.set_texture_unit(0);
	texture.upload();

	shader_id = shader::get("texturedphongmaterial");
}

GLuint TexturedPhongMaterial::bind() {
	glUseProgram(shader_id);
	texture.bind_to_shader(shader::uniform(shader_id, "colormap"));
	return shader_id;
}