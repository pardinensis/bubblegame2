#include "shadowmap.hpp"

ShadowMap::ShadowMap(uint width, uint height) : width(width), height(height), texture_unit(0) {
	glGenFramebuffers(1, &fbo_id);

	glGenTextures(1, &tex_id);
    glBindTexture(GL_TEXTURE_2D, tex_id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex_id, 0);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
}

ShadowMap::~ShadowMap() {
	glDeleteTextures(1, &tex_id);
	glDeleteFramebuffers(1, &fbo_id);
}

uint ShadowMap::get_texture_unit() const {
	return texture_unit;
}
void ShadowMap::set_texture_unit(uint texture_unit) {
	this->texture_unit = texture_unit;
}

uint ShadowMap::get_width() const {
    return width;
}

uint ShadowMap::get_height() const {
    return height;
}

void ShadowMap::bind_for_writing() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_id);
}

void ShadowMap::bind_for_reading() {
    glActiveTexture(GL_TEXTURE0 + texture_unit);
    glBindTexture(GL_TEXTURE_2D, tex_id);
}

void ShadowMap::bind_fbo() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_id);
	glReadBuffer(GL_COLOR_ATTACHMENT0 + texture_unit);
}