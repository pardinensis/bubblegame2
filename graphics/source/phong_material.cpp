#include "phong_material.hpp"

#include "shader.hpp"

PhongMaterial::PhongMaterial(const glm::vec3& color) : color(color) {
	static bool initialized = false;
	if (!initialized) {
		shader::create("phongmaterial", "pos_norm.vs.glsl", GL_VERTEX_SHADER);
		shader::create("phongmaterial", "ds_geometry_plain.fs.glsl", GL_FRAGMENT_SHADER);
		initialized = true;
	}
	shader_id = shader::get("phongmaterial");
}

GLuint PhongMaterial::bind() {
	glUseProgram(shader_id);
	glUniform3f(shader::uniform(shader_id, "color"), color.x, color.y, color.z);
	return shader_id;
}