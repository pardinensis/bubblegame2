#include "material.hpp"

GLuint Material::bind() {
	glUseProgram(shader_id);
	return shader_id;
}

Material::~Material() {}