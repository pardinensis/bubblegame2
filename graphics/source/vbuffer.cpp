#include "vbuffer.hpp"

#include "math.hpp"


VBuffer::VBuffer(uint width, uint height, uint depth) : width(width), height(height), depth(depth) {
	if (depth == 0 || !is_exp2(depth))
		ERROR("depth has to be a power of 2");

	glGenFramebuffers(1, &fbo_id);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);

	glGenTextures(1, &vbuf_texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, vbuf_texture);

	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, width, height, depth, 0, GL_RGBA, GL_FLOAT, nullptr);

	// evtl glFramebufferTextureLayer??
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, vbuf_texture, 0);

	GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (Status != GL_FRAMEBUFFER_COMPLETE) {
		printf("FB error, status: 0x%x\n", Status);
		return;
	}

	// restore default FBO
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

VBuffer::~VBuffer() {
	glDeleteTextures(1, &vbuf_texture);
	glDeleteFramebuffers(1, &fbo_id);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void VBuffer::bind_for_reading() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glActiveTexture(GL_TEXTURE0);		
	glBindTexture(GL_TEXTURE_2D, vbuf_texture);
}

void VBuffer::bind_for_writing() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_id);
}