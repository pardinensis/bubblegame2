#include "fog.hpp"

#include "shader.hpp"

static const std::string vertex_shader_file = "fog.vs.glsl";
static const std::string fragment_shader_file = "fog.fs.glsl";
static uint shader_id = -1;


void Fog::init_shader() {
	shader::create("fog", vertex_shader_file, GL_VERTEX_SHADER);
	shader::create("fog", fragment_shader_file, GL_FRAGMENT_SHADER);
	shader_id = shader::get("fog");
}

uint Fog::get_shader() {
	return shader_id;
}

uint Fog::bind_shader() {
	glUseProgram(shader_id);
	return shader_id;
}
