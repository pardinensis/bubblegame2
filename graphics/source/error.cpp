#include "error.hpp"

#include "common_includes.hpp"
#include "backtrace.hpp"

#include <sstream>


namespace Error {
	void checkGL(const char* filename, int line) {
		GLenum err = glGetError();
		if (err != GL_NO_ERROR) {
			std::stringstream ss;
			ss << "\nGL Error: ";
			switch(err) {
				case GL_INVALID_ENUM: ss << "GL_INVALID_ENUM"; break;
				case GL_INVALID_VALUE: ss << "GL_INVALID_VALUE"; break;
				case GL_INVALID_OPERATION: ss << "GL_INVALID_OPERATION"; break;
				case GL_INVALID_FRAMEBUFFER_OPERATION: ss << "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
				case GL_OUT_OF_MEMORY: ss << "GL_OUT_OF_MEMORY"; break;
				default: ss << "WHAT HAVE YOU DONE?";
			}
			ss << " in file " << filename << " (line: " << line << ")\n";
			throw std::runtime_error(ss.str());
		}
	}

	void checkSDL(void* object, const std::string& message, const char* filename, int line) {
		if (object == nullptr) {
			std::stringstream ss;
			ss << "\nSDL Error: " << message << " in file " << filename << " (line: " << line << ")\n";
			throw std::runtime_error(ss.str());
		}
	}

	void error(const std::string& message, const char* filename, int line) {
		std::cerr << "=================================================================" << std::endl;
		std::cerr << " Error: " << message << " in file " << filename << " (line: " << line << ")";
		std::cerr << std::endl << std::endl;
		print_stacktrace();
		std::cerr << "=================================================================" << std::endl;
		exit(1);
	}

	void error_callback(GLenum, GLenum, GLuint, GLenum severity, GLsizei, const GLchar *message, void*) {
		std::cerr << "=================================================================" << std::endl;
		std::cerr << " GL Error: " << message << " Severity: ";
		switch (severity) {
			case GL_DEBUG_SEVERITY_LOW:    std::cerr << "low"; break;
			case GL_DEBUG_SEVERITY_MEDIUM: std::cerr << "medium"; break;
			case GL_DEBUG_SEVERITY_HIGH:   std::cerr << "high"; break;
		}
		std::cerr << std::endl << std::endl;
		print_stacktrace();
		std::cerr << "=================================================================" << std::endl;
		exit(1);
	}

	void warning(const std::string& message, const char* filename, int line) {
		std::cerr << "Warning: '" << message << "' in file " << filename << " (line: " << line << ")\n";
	}
}