#include "directional_light.hpp"

#include "shader.hpp"

static uint loc_color;
static uint loc_ambient_color;
static uint loc_direction;
static uint loc_intensity;
static uint loc_ambient_intensity;

static const std::string vertex_shader_file = "ds_direct.vs.glsl";
static const std::string fragment_shader_file = "ds_direct.fs.glsl";
static uint shader_id = -1;


DirectionalLight::DirectionalLight(const glm::vec3& color, const glm::vec3& ambient_color,
		const glm::vec3& direction, float intensity, float ambient_intensity)
	: color(color), ambient_color(ambient_color), direction(direction),
		intensity(intensity), ambient_intensity(ambient_intensity) {}

void DirectionalLight::init_shader() {
	shader::create("directlight", vertex_shader_file, GL_VERTEX_SHADER);
	shader::create("directlight", fragment_shader_file, GL_FRAGMENT_SHADER);
	shader_id = shader::get("directlight");

	loc_color = shader::uniform(shader_id, "light_color");
	loc_ambient_color = shader::uniform(shader_id, "light_ambient_color");
	loc_direction = shader::uniform(shader_id, "light_direction");
	loc_intensity = shader::uniform(shader_id, "light_intensity");
	loc_ambient_intensity = shader::uniform(shader_id, "light_ambient_intensity");
}

uint DirectionalLight::get_shader() {
	return shader_id;
}

uint DirectionalLight::bind_shader() {
	glUseProgram(shader_id);
	return shader_id;
}

void DirectionalLight::upload() {
	if (shader_id == (uint)-1)
		ERROR("directional light shader has not been initialized");

	glUniform3f(loc_color, color.x, color.y, color.z);
	glUniform3f(loc_ambient_color, ambient_color.x, ambient_color.y, ambient_color.z);
	glUniform3f(loc_direction, direction.x, direction.y, direction.z);
	glUniform1f(loc_intensity, intensity);
	glUniform1f(loc_ambient_intensity, ambient_intensity);
}