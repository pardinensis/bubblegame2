#include "vao.hpp"

#include "math.hpp"

VAO::VAO(uint buffer_contents) :
	primitive_type(GL_TRIANGLES),
	n_vertices(0),
	n_indices(0),
	n_vertex_buffers(0),
	buffer_locations(N_BUFFER_CONTENT_TYPES, -1) {

	for (int i = 0; i < N_BUFFER_CONTENT_TYPES; ++i) {
		if (buffer_contents & (1 << i)) {
			buffer_locations[i] = n_vertex_buffers;
			++n_vertex_buffers;
		}
	}

	glGenVertexArrays(1, &vao_id);
	vbo_ids.reserve(n_vertex_buffers);
	glGenBuffers(n_vertex_buffers, vbo_ids.data());
	glGenBuffers(1, &ib_id);
}

VAO::~VAO() {
	glDeleteBuffers(1, &ib_id);
	glDeleteBuffers(n_vertex_buffers, vbo_ids.data());
	glDeleteVertexArrays(1, &vao_id);
}

void VAO::change_vertex_data(uint buffer_content, GLenum type, uint n_vertices, uint n_dimensions, const void* data) {
	int location = buffer_locations.at(log2(buffer_content));
	if (location < 0) {
		ERROR("Mesh::change_vertex_data: invalid or unavailable buffer_content");
	}

	if (this->n_vertices == 0) {
		this->n_vertices = n_vertices;
	} else if (this->n_vertices != n_vertices) {
		ERROR("Mesh::change_vertex_data: n_vertices don't match");
	}

	uint size_in_bytes = sizeof(float) * n_vertices * n_dimensions;

	glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[location]);
	glBufferData(GL_ARRAY_BUFFER, size_in_bytes, data, GL_STATIC_DRAW);
	glEnableVertexAttribArray(location);
	glVertexAttribPointer(location, n_dimensions, type, GL_FALSE, 0, 0);
}

void VAO::change_index_data(uint n_indices, GLenum primitive_type, const void* data) {
	this->n_indices = n_indices;
	this->primitive_type = primitive_type;

	uint size_in_bytes = sizeof(uint) * n_indices;

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib_id);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size_in_bytes, data, GL_STATIC_DRAW);
}

void VAO::bind() {
	glBindVertexArray(vao_id);
}

void VAO::unbind() {
	glBindVertexArray(0);
}

void VAO::draw() {
	glBindVertexArray(vao_id);
	glDrawElements(primitive_type, n_indices, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}