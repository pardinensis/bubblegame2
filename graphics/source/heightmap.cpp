#include "heightmap.hpp"

#include "heightmap_material.hpp"
#include "frustum.hpp"
#include "io.hpp"
#include "math.hpp"
#include "shader.hpp"

std::vector<glm::vec2> cpu_vertex_buffer;
std::vector<std::vector<uint>> cpu_index_buffers;

HMNode::HMNode() {

}

void HMNode::calculate_bounding_box(float size, const glm::vec3& scale) {
	child_ll->calculate_bounding_box(size, scale);
	child_lh->calculate_bounding_box(size, scale);
	child_hl->calculate_bounding_box(size, scale);
	child_hh->calculate_bounding_box(size, scale);

	float x_l = min(child_ll->aabb_l.x, child_lh->aabb_l.x);
	float y_l = min(child_ll->aabb_l.y, child_lh->aabb_l.y, child_hl->aabb_l.y, child_hh->aabb_l.y);
	float z_l = min(child_ll->aabb_l.z, child_hl->aabb_l.z);

	float x_h = max(child_hl->aabb_h.x, child_hh->aabb_h.x);
	float y_h = max(child_ll->aabb_h.y, child_lh->aabb_h.y, child_hl->aabb_h.y, child_hh->aabb_h.y);
	float z_h = max(child_lh->aabb_h.z, child_hh->aabb_h.z);

	aabb_l = glm::vec3(x_l, y_l, z_l);
	aabb_h = glm::vec3(x_h, y_h, z_h);
}

void HMNode::calculate_bounding_sphere() {
	child_ll->calculate_bounding_sphere();
	child_lh->calculate_bounding_sphere();
	child_hl->calculate_bounding_sphere();
	child_hh->calculate_bounding_sphere();

	bs_c = 0.25f * (child_ll->bs_c + child_lh->bs_c + child_hl->bs_c + child_hh->bs_c);
	float r_ll = glm::distance(child_ll->bs_c, bs_c) + child_ll->bs_r;
	float r_lh = glm::distance(child_lh->bs_c, bs_c) + child_lh->bs_r;
	float r_hl = glm::distance(child_hl->bs_c, bs_c) + child_hl->bs_r;
	float r_hh = glm::distance(child_hh->bs_c, bs_c) + child_hh->bs_r;
	bs_r = max(r_ll, r_lh, r_hl, r_hh);
}

void HMNode::draw(GLuint prog, const Frustum& frustum) {
	if (frustum.check_sphere(bs_c, bs_r)) {
		child_ll->draw(prog, frustum);
		child_lh->draw(prog, frustum);
		child_hl->draw(prog, frustum);
		child_hh->draw(prog, frustum);
	}
}



uint get_index(uint x, uint y) {
	return x + (CHUNK_SIZE + 1) * y;
}

void HMChunk::init_cpu_buffers() {
	// vertex buffer
	for (uint y = 0; y < CHUNK_SIZE + 1; ++y) {
		for (uint x = 0; x < CHUNK_SIZE + 1; ++x) {
			cpu_vertex_buffer.push_back(glm::vec2(x, y));
		}
	}

	// index buffers
	for (uint lod = CHUNK_SIZE; lod > 0; lod >>= 1) {
		uint stepsize = CHUNK_SIZE / lod;

		std::vector<uint> buf;
		for (uint y = 0; y < CHUNK_SIZE; y += stepsize) {
			if (y != 0) {
				buf.push_back(get_index(CHUNK_SIZE, y));
				buf.push_back(get_index(         0, y));
			}
			for (uint x = 0; x < CHUNK_SIZE + 1; x += stepsize) {
				buf.push_back(get_index(x, y           ));
				buf.push_back(get_index(x, y + stepsize));
			}
		}
		cpu_index_buffers.push_back(buf);
	}
}

HMChunk::HMChunk(glm::vec2 offset, Texture* texture) : offset(offset), texture(texture) {
	if (cpu_index_buffers.empty())
		ERROR("cpu_index_buffers not initialized");

	std::vector<uint>& index_buffer = cpu_index_buffers[0];

	vao = new VAO(VAO::BUFFER_CONTENT_TYPE_POSITION);
	vao->bind();
	vao->change_vertex_data(VAO::BUFFER_CONTENT_TYPE_POSITION, GL_FLOAT,
		cpu_vertex_buffer.size(), 2, cpu_vertex_buffer.data());
	vao->change_index_data(index_buffer.size(), GL_TRIANGLE_STRIP, index_buffer.data());
	vao->unbind();
}

void HMChunk::draw(GLuint prog, const Frustum& frustum) {
	if (frustum.check_sphere(bs_c, bs_r)) {
		glUniform2f(shader::uniform(prog, "offset"), offset.x, offset.y);
		vao->draw();
	}
}

void HMChunk::calculate_bounding_box(float size, const glm::vec3& scale) {
	float x_l = offset.x;
	float z_l = offset.y;

	float x_h = x_l + CHUNK_SIZE;
	float z_h = z_l + CHUNK_SIZE;

	float y_l = POSITIVE_INFINITY;
	float y_h = NEGATIVE_INFINITY;
	for (int z = 0; z < CHUNK_SIZE + 1; ++z) {
		for (int x = 0; x < CHUNK_SIZE + 1; ++x) {
			float f = texture->get_pixel(x + x_l, z + z_l).get_r();
			if (f < y_l)
				y_l = f;
			if (f > y_h)
				y_h = f;
		}
	}

	aabb_l = glm::vec3(x_l / size, y_l, z_l / size) * scale;
	aabb_h = glm::vec3(x_h / size, y_h, z_h / size) * scale;
}

void HMChunk::calculate_bounding_sphere() {
	bs_c = (aabb_l + aabb_h) * 0.5f;
	bs_r = glm::distance(aabb_l, bs_c);
}


void Heightmap::smooth(Texture& raw, Texture& filtered) {
	for (uint y = 0; y < size; ++y) {
		uint y_low = (y < 2) ? 0 : y - 2;
		uint y_high = (y > size - 3) ? size - 1 : y + 2;
		for (uint x = 0; x < size; ++x) {
			uint x_low = (x < 2) ? x : x - 2;
			uint x_high = (x > size - 3) ? x : x + 2;
			float y_sum = raw.get_pixel(x, y).get_r();
			y_sum += raw.get_pixel(x_low, y).get_r();
			y_sum += raw.get_pixel(x_high, y).get_r();
			y_sum += raw.get_pixel(x, y_low).get_r();
			y_sum += raw.get_pixel(x, y_high).get_r();
			filtered.set_pixel(x, y, Pixel(y_sum / 5.f, 0, 0, 1));
		}
	}
}


Heightmap::Heightmap(const std::string& height_texture_file, Texture* color_texture, glm::vec3 scale) {
	set_model_matrix(glm::scale(scale));

	HMChunk::init_cpu_buffers();

	{ // read heightmap texture
		height_texture = new Texture(height_texture_file);

		uint width = height_texture->get_width();
		uint height = height_texture->get_height();
		if (width > 1 && width == height && is_exp2(width - 1))
			size = width;
		else
			ERROR("heightmap texture has to be quadratic and of size 2^n+1");

		Texture tmp(height_texture->get_width(), height_texture->get_height());
		smooth(*height_texture, tmp);
		smooth(tmp, *height_texture);
		height_texture->set_texture_unit(0);
		height_texture->upload();
	}

	{ // create normal_texture
		normal_texture = new Texture(size, size);
		float sizem1 = size - 1;
		for (uint y = 0; y < size; ++y) {
			uint y_low = (y == 0) ? y : y - 1;
			uint y_high = (y == size - 1) ? y : y + 1;
			for (uint x = 0; x < size; ++x) {
				uint x_low = (x == 0) ? x : x - 1;
				uint x_high = (x == size - 1) ? x : x + 1;
				glm::vec3 vx_low(x_low / sizem1, height_texture->get_pixel(x_low, y).get_r(), y / sizem1);
				glm::vec3 vx_high(x_high / sizem1, height_texture->get_pixel(x_high, y).get_r(), y / sizem1);
				glm::vec3 vy_low(x / sizem1, height_texture->get_pixel(x, y_low).get_r(), y_low / sizem1);
				glm::vec3 vy_high(x / sizem1, height_texture->get_pixel(x, y_high).get_r(), y_high / sizem1);
				glm::vec3 to_x = vx_high - vx_low;
				glm::vec3 to_y = vy_high - vy_low;
				glm::vec3 n11 = glm::normalize(glm::cross(to_y, to_x));
				glm::vec3 n01 = (n11 + glm::vec3(1, 1, 1)) * 0.5f;
				normal_texture->set_pixel(x, y, Pixel(n01.x, n01.y, n01.z, 1.f));
			}
		}
		normal_texture->set_texture_unit(1);
		normal_texture->upload();
	}

	{ // create chunks
		for (uint y = 0; y < size - 1; y += CHUNK_SIZE) {
			for (uint x = 0; x < size - 1; x += CHUNK_SIZE) {
				HMChunk chunk(glm::vec2(x, y), height_texture);
				chunks.push_back(chunk);
			}
		}
	}

	color_texture->set_texture_unit(2);
	color_texture->set_mipmapping(true);
	color_texture->upload();
	mat = new HeightmapMaterial(height_texture, normal_texture, color_texture);
	
	// create quad tree structure
	// quadtree = createQuadtree((size - 1) / CHUNK_SIZE, 0, 0);

	// quadtree->calculate_bounding_box(size, scale);
	// quadtree->calculate_bounding_sphere();
}

Heightmap::~Heightmap() {
	delete height_texture;
	delete normal_texture;
	delete mat;

	// deleteQuadtree(quadtree, (size - 1) / CHUNK_SIZE);
}

/*HMNode* Heightmap::createQuadtree(uint level, uint idx_x, uint idx_y) {
	if (level == 1)
		return &(chunks[idx_x + ((size - 1) / CHUNK_SIZE) * idx_y]);

	level /= 2;

	HMNode* node = new HMNode;
	node->child_ll = createQuadtree(level, idx_x,         idx_y        );
	node->child_lh = createQuadtree(level, idx_x,         idx_y + level);
	node->child_hl = createQuadtree(level, idx_x + level, idx_y        );
	node->child_hh = createQuadtree(level, idx_x + level, idx_y + level);
	return node;
}

void Heightmap::deleteQuadtree(HMNode* node, uint level) {
	if (level > 1) {
		deleteQuadtree(node->child_ll, level / 2);
		deleteQuadtree(node->child_lh, level / 2);
		deleteQuadtree(node->child_hl, level / 2);
		deleteQuadtree(node->child_hh, level / 2);
		delete node;
	}
}*/

// void Heightmap::draw(GLuint prog, const Frustum& frustum) {
// 	height_texture->bind_to_shader(shader::uniform(prog, "heightmap"));
// 	normal_texture->bind_to_shader(shader::uniform(prog, "normalmap"));
// 	glUniform1f(shader::uniform(prog, "size"), size);
// 	quadtree->draw(prog, frustum);
// }


void Heightmap::draw(GLuint shader_id) {
	glUniformMatrix4fv(shader::uniform(shader_id, "model_matrix"), 1, GL_FALSE, glm::value_ptr(model_matrix));
	glUniformMatrix4fv(shader::uniform(shader_id, "normal_matrix"), 1, GL_FALSE, glm::value_ptr(normal_matrix));
	glUniform1f(shader::uniform(shader_id, "size"), size);
	for (HMChunk& chunk : chunks) {
		glUniform2f(shader::uniform(shader_id, "offset"), chunk.offset.x, chunk.offset.y);
		chunk.vao->draw();
	}
}