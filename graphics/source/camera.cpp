#include "camera.hpp"


Camera::Camera(const std::string& name) : name(name) {}
Camera::Camera(const std::string& name, const glm::vec3& pos, const glm::vec3& dir, const glm::vec3& up) : 
	name(name), pos(pos), dir(dir), up(up) {}

void Camera::key_pressed(const SDL_Keycode& keycode) {}
void Camera::key_released(const SDL_Keycode& keycode) {}
void Camera::mouse_moved(int xrel, int yrel) {}

void Camera::update(float time_interval) {
	recalculate_matrix();
}

void Camera::update_camera() {
	float time_interval = timer.measure();
	timer.restart();
	update(time_interval);
}

void Camera::recalculate_matrix() {
	view_matrix = glm::lookAt(pos, pos + dir, up);
}