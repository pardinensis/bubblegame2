/*

#include "common_includes.hpp"

// graphics
#include "debug_camera.hpp"
#include "gbuffer.hpp"
#include "heightmap.hpp"
#include "directional_light.hpp"
#include "main_window.hpp"
#include "mesh.hpp"
#include "point_light.hpp"
#include "projection.hpp"
#include "shader.hpp"
#include "shader_uploads.hpp"
#include "texture.hpp"

// utils
#include "config_parser.hpp"
#include "fps.hpp"
#include "random.hpp"
#include "timer.hpp"


#include <iomanip>
#include <cstdlib>


void render_geometry(GBuffer& gbuffer, const Camera& cam, const Projection& proj,
		Heightmap& heightmap, TextureRGB& grass, DrawElement& balloon) {
	// prepare
	gbuffer.bind_for_geom_pass();
	glDepthMask(GL_TRUE);
	glClearColor(0.f, 0.f, 0.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	// render heightmap
	GLuint prog = shader::use("heightmap");
	shader::upload_view_matrix(prog, cam);
	shader::upload_proj_matrix(prog, proj);
	glUniform1i(shader::uniform(prog, "colormap"), grass.bind_to_texture_unit(2));
	glUniform1f(shader::uniform(prog, "tc_scale"), 25);
	Frustum frustum(proj.proj_matrix * cam.view_matrix);		
	heightmap.draw(prog, frustum);

	// render balloon
	prog = shader::use("balloon");
	glm::mat4 model_matrix;
	shader::upload_model_and_normal_matrix(prog, model_matrix);
	shader::upload_view_matrix(prog, cam);
	shader::upload_proj_matrix(prog, proj);
	glUniform3f(shader::uniform(prog, "color"), 0.2f, 0.9f, 0.5f);
	for (auto mesh : balloon.meshes) {
		mesh->bind();
		mesh->draw();
		mesh->unbind();
	}

	// finish
	glDepthMask(GL_FALSE);
}

void render_point_lights(GBuffer& gbuffer, uint window_width, uint window_height, const Camera& cam, const Projection& proj,
		std::vector<PointLight>& point_lights, const DrawElement& sphere) {

	glEnable(GL_STENCIL_TEST);

	for (auto& pl : point_lights) {
		// stencil pass
		GLuint prog = shader::use("null");
		gbuffer.bind_for_stencil_pass();
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glClear(GL_STENCIL_BUFFER_BIT);

		glStencilFunc(GL_ALWAYS, 0, 0);
		glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
		glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

		glm::mat4 pl_scale = glm::scale(glm::vec3(pl.size, pl.size, pl.size));
		glm::mat4 pl_translate = glm::translate(pl.position);
		shader::upload_model_matrix(prog, pl_translate * pl_scale);
		shader::upload_view_matrix(prog, cam);
		shader::upload_proj_matrix(prog, proj);
		sphere.meshes[0]->bind();
		sphere.meshes[0]->draw();
		sphere.meshes[0]->unbind();
		
		// point light pass
		prog = PointLight::bind_shader();
		gbuffer.bind_for_light_pass();
      		
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glStencilFunc(GL_NOTEQUAL, 0, 0xFF);
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_ONE, GL_ONE);
		
		shader::upload_model_matrix(prog, pl_translate * pl_scale);
		shader::upload_view_matrix(prog, cam);
		shader::upload_proj_matrix(prog, proj);
		glUniform1i(shader::uniform(prog, "tex_pos"), 0);
		glUniform1i(shader::uniform(prog, "tex_norm"), 1);
		glUniform1i(shader::uniform(prog, "tex_col"), 2);
		glUniform1i(shader::uniform(prog, "tex_depth"), 3);
		glUniform2f(shader::uniform(prog, "screensize"), window_width, window_height);
		pl.upload();
		
		sphere.meshes[0]->bind();
		sphere.meshes[0]->draw();
		sphere.meshes[0]->unbind();
		
		glCullFace(GL_BACK);
		glDisable(GL_BLEND);
		
	}

	glDisable(GL_STENCIL_TEST);
}

void render_direct_light(GBuffer& gbuffer, uint window_width, uint window_height,
		DirectionalLight& sun, DrawElement& ssquad) {
	gbuffer.bind_for_light_pass();
	
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);
	CHECK_GL_ERROR();

	GLuint prog = DirectionalLight::bind_shader();
	CHECK_GL_ERROR();
	glUniform1i(shader::uniform(prog, "tex_norm"), 1);
	glUniform1i(shader::uniform(prog, "tex_col"), 2);
	glUniform1i(shader::uniform(prog, "tex_depth"), 3);
	glUniform2f(shader::uniform(prog, "screensize"), window_width, window_height);
	// sun.init_locs(prog);
	CHECK_GL_ERROR();
	sun.upload();
	CHECK_GL_ERROR();
	ssquad.meshes[0]->bind();
	ssquad.meshes[0]->draw();
	ssquad.meshes[0]->unbind();

	glDisable(GL_BLEND);

	CHECK_GL_ERROR();
}

int main() {
	Config::parse();

	MainWindow main_window;

	shader::create("balloon", "pos_norm.vs.glsl", GL_VERTEX_SHADER);
	shader::create("balloon", "ds_geometry_plain.fs.glsl", GL_FRAGMENT_SHADER);

	shader::create("heightmap", "heightmap.vs.glsl", GL_VERTEX_SHADER);
	shader::create("heightmap", "ds_geometry_textured.fs.glsl", GL_FRAGMENT_SHADER);
	
	DirectionalLight::init_shader();
	PointLight::init_shader();

	shader::create("null", "ds_null.vs.glsl", GL_VERTEX_SHADER);
	shader::create("null", "ds_null.fs.glsl", GL_FRAGMENT_SHADER);

	uint window_width = Config::get<int>("window width");
	uint window_height = Config::get<int>("window height");
	GBuffer gbuffer(window_width, window_height);

	Heightmap heightmap("heightmap513.png", glm::vec3(50, 10, 50));

	DrawElement ssquad("../graphics/models/quad.obj");
	DrawElement sphere("../graphics/models/sphere.obj");

	DrawElement balloon("../graphics/models/balloon.obj");

	glm::vec3 sun_color(0.93, 0.82, 0.58);
	glm::vec3 sun_ambient_color(0.59, 0.73, 0.88);
	float sun_intensity = 0.3;
	float sun_ambient_intensity = 0.1;
	glm::vec3 sun_direction(6, -9, 2);
	DirectionalLight sun(sun_color, sun_ambient_color, glm::normalize(sun_direction),
		sun_intensity, sun_ambient_intensity);

	glm::vec3 pl_color(0.9, 0.8, 0.4);
	float pl_intensity = 3.0;
	float pl_size = 30;
	std::vector<PointLight> point_lights;
	for (int i = 0; i < 20; ++i) {
		glm::vec3 pl_pos(Random::get_float(0, 50), Random::get_float(7, 15), Random::get_float(0, 50));
		point_lights.push_back(PointLight(pl_color, pl_pos, pl_intensity, pl_size));
	}

	glActiveTexture(GL_TEXTURE2);
	TextureRGB grass("grass01.png");

	DebugCamera cam("debug");
	cam.pos = glm::vec3(1, 3.9, 6.6);
	cam.dir = glm::normalize(glm::vec3(4, -.1, 3));
	cam.up = glm::vec3(0, 1, 0);
	main_window.attach_camera(&cam);


	Projection proj("default");
	proj.set(window_width, window_height, 0.1, 1000, 60 * M_PI/180);

	SDL_SetRelativeMouseMode(SDL_TRUE);

	CHECK_GL_ERROR();
		
	Timer timer;
	while (main_window.active) {
		main_window.process_events();
		main_window.start_loop();

		float time_interval = timer.measure();
		timer.restart();

		cam.update(time_interval);


		// prepare gbuffer
		gbuffer.start_frame();
		

		// geometry pass
		render_geometry(gbuffer, cam, proj, heightmap, grass, balloon);
	CHECK_GL_ERROR();
		

		// point light pass
		render_point_lights(gbuffer, window_width, window_height, cam, proj, point_lights, sphere);
	CHECK_GL_ERROR();
		

		// direct light pass
		render_direct_light(gbuffer, window_width, window_height, sun, ssquad);
	CHECK_GL_ERROR();

		// final pass
		gbuffer.bind_for_final_pass();
		glBlitFramebuffer(0, 0, window_width, window_height, 
		                  0, 0, window_width, window_height, GL_COLOR_BUFFER_BIT, GL_LINEAR);
	CHECK_GL_ERROR();











#if 0
		// === direct light ===		
		prog = shader::use("directlight");
		//glUniform1i(shader::uniform(prog, "tex_pos"), 0);
		glUniform1i(shader::uniform(prog, "tex_norm"), 1);
		glUniform1i(shader::uniform(prog, "tex_col"), 2);
		glUniform1i(shader::uniform(prog, "tex_depth"), 3);
		glUniform2f(shader::uniform(prog, "screensize"), window_width, window_height);
		sun.init_locs(prog);
		sun.upload();
		ssquad.meshes[0]->bind();
		ssquad.meshes[0]->draw();
		ssquad.meshes[0]->unbind();
		
#endif

#if 0
		// === point lights ===
		prog = shader::use("pointlight");
		glUniform1i(shader::uniform(prog, "tex_pos"), 0);
		glUniform1i(shader::uniform(prog, "tex_norm"), 1);
		glUniform1i(shader::uniform(prog, "tex_col"), 2);
		glUniform1i(shader::uniform(prog, "tex_depth"), 3);
		glUniform2f(shader::uniform(prog, "screensize"), window_width, window_height);
		shader::upload_view_matrix(prog, cam);
		shader::upload_proj_matrix(prog, proj);
		PointLight::init_locs(prog);
		for (PointLight& pl : point_lights) {
			pl.upload();
			glm::mat4 pl_scale = glm::scale(glm::vec3(pl.size, pl.size, pl.size));
			glm::mat4 pl_translate = glm::translate(pl.position);
			shader::upload_model_matrix(prog, pl_translate * pl_scale);
			sphere.meshes[0]->bind();
			sphere.meshes[0]->draw();
			sphere.meshes[0]->unbind();
		}
#endif



		static double t_avg = 0.01;
		static double t_output = 0;
		if (time_interval < 100) {
			t_avg += 0.1 * (time_interval - t_avg);
			t_output += time_interval;
			if (t_output >= 1) {
				std::cout << "fps: " << (int)(1 / t_avg) << std::endl;
				t_output = 0;
			}
		}

		main_window.end_loop();
	}
	
	return 0;
}
*/