#include "buffer.hpp"

Buffer::Buffer() : id(0), texture_unit(0) {
	//
}

GLuint Buffer::create_buffer_rgb(uint width, uint height) {
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, NULL);

	return id;
}

void Buffer::bind(GLuint loc) {
	glActiveTexture(GL_TEXTURE0 + texture_unit);

	glBindTexture(GL_TEXTURE_2D, id);
	glUniform1i(loc, texture_unit);
}