#include "mesh.hpp"

#include "mesh_loader.hpp"
#include "shader.hpp"

Mesh::Mesh(const std::vector<VAO*>& vaos, Material* mat) 
	: vaos(vaos), mat(mat) {}

Mesh::Mesh(const std::string& vao_file, Material* mat)
	: Mesh(MeshLoader::load_vaos(vao_file), mat) {}

Mesh::~Mesh() {}

void Mesh::get_model_matrix(glm::mat4& model_matrix) const {
	model_matrix = this->model_matrix;
}

void Mesh::set_model_matrix(const glm::mat4& model_matrix) {
	this->model_matrix = model_matrix;
	normal_matrix = glm::transpose(glm::inverse(model_matrix));
}

Material* Mesh::get_material() const {
	return mat;
}

void Mesh::set_material(Material* material) {
	mat = material;
}

void Mesh::draw(GLuint shader_id) {
	glUniformMatrix4fv(shader::uniform(shader_id, "model_matrix"), 1, GL_FALSE, glm::value_ptr(model_matrix));
	glUniformMatrix4fv(shader::uniform(shader_id, "normal_matrix"), 1, GL_FALSE, glm::value_ptr(normal_matrix));
	for (VAO* vao : vaos) {
		vao->draw();
	}
}
