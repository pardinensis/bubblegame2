#include "renderer.hpp"

#include "directional_light.hpp"
#include "fog.hpp"
#include "mesh.hpp"
#include "mesh_loader.hpp"
#include "shader.hpp"
#include "vao.hpp"

Renderer::Renderer(uint width, uint height) : gbuffer_width(width), gbuffer_height(height), shadow(nullptr), flags(0) {
	gbuffer = new GBuffer(gbuffer_width, gbuffer_height);
	vbuffer = new VBuffer(160, 90, 128);

	DirectionalLight::init_shader();
	Fog::init_shader();

	shader::create("debug", "ds_direct.vs.glsl", GL_VERTEX_SHADER);
	shader::create("debug", "ds_debug.fs.glsl", GL_FRAGMENT_SHADER);

	ssquad = MeshLoader::load_vaos("../graphics/models/quad.obj")[0];

	fog_density = 3;
}

Renderer::~Renderer() {
	delete ssquad;
	delete gbuffer;
}

void Renderer::render_geometry(const glm::mat4& view_matrix, const glm::mat4& proj_matrix) {
	glDepthMask(GL_TRUE);
	glClearColor(0.f, 0.f, 0.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	auto mat_map = current_scene->material_map;
	for (auto it = mat_map.begin(); it != mat_map.end(); ++it) {
		GLuint shader_id = it->first->bind();
		glUniformMatrix4fv(shader::uniform(shader_id, "view_matrix"),
			1, GL_FALSE, glm::value_ptr(view_matrix));
		glUniformMatrix4fv(shader::uniform(shader_id, "proj_matrix"),
			1, GL_FALSE, glm::value_ptr(proj_matrix));
		for (auto mesh : it->second) {
			mesh->draw(shader_id);
		}
	}

	glDepthMask(GL_FALSE);
}

void Renderer::render_point_lights() {
}

void Renderer::render_directional_light() {
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);


	GLuint shader_id = DirectionalLight::bind_shader();
	glUniform1i(shader::uniform(shader_id, "tex_pos"), GBuffer::GBUFFER_TEXTURE_POS);
	glUniform1i(shader::uniform(shader_id, "tex_norm"), GBuffer::GBUFFER_TEXTURE_NORM);
	glUniform1i(shader::uniform(shader_id, "tex_col"), GBuffer::GBUFFER_TEXTURE_COL);
	glUniform1i(shader::uniform(shader_id, "tex_depth"), GBuffer::TEXTURE_DEPTH_STENCIL);
	glUniform2f(shader::uniform(shader_id, "screensize"), gbuffer_width, gbuffer_height);
	glUniform2f(shader::uniform(shader_id, "shadowmapsize"), shadow->shadowmap.get_width(), shadow->shadowmap.get_height());

	glm::mat4 light_vp_matrix = shadow->proj_matrix * shadow->view_matrix;
	glUniformMatrix4fv(shader::uniform(shader_id, "light_matrix"), 1, GL_FALSE, glm::value_ptr(light_vp_matrix));
	shadow->shadowmap.set_texture_unit(5);
	glUniform1i(shader::uniform(shader_id, "tex_shadow"), 5);

	current_scene->directional_light->upload();

	glUniform3f(shader::uniform(shader_id, "sky_color"), 0.55, 0.8, 1.0);

	ssquad->draw();

	glDisable(GL_BLEND);
}

void Renderer::render_final_pass() {
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	GLuint shader_id = Fog::bind_shader();
	glUniform1i(shader::uniform(shader_id, "tex_depth"), GBuffer::TEXTURE_DEPTH_STENCIL);
	glUniform1i(shader::uniform(shader_id, "tex_shaded"), GBuffer::TEXTURE_FINAL);
	glUniform1f(shader::uniform(shader_id, "near"), current_scene->active_projection->nearplane);
	glUniform1f(shader::uniform(shader_id, "far"), current_scene->active_projection->farplane);
	glUniform2f(shader::uniform(shader_id, "screensize"), gbuffer_width, gbuffer_height);

	glm::mat4 light_vp_matrix = shadow->proj_matrix * shadow->view_matrix;
	glUniformMatrix4fv(shader::uniform(shader_id, "light_matrix"), 1, GL_FALSE, glm::value_ptr(light_vp_matrix));
	shadow->shadowmap.set_texture_unit(5);
	glUniform1i(shader::uniform(shader_id, "tex_shadow"), 5);
	glUniform2f(shader::uniform(shader_id, "shadowmapsize"), shadow->shadowmap.get_width(), shadow->shadowmap.get_height());

	glm::mat4 view_matrix = current_scene->active_camera->view_matrix;
	glm::mat4 proj_matrix = current_scene->active_projection->proj_matrix;
	glm::mat4 inv_vp_matrix = glm::inverse(proj_matrix * view_matrix);
	glUniformMatrix4fv(shader::uniform(shader_id, "inv_vp_matrix"), 1, GL_FALSE, glm::value_ptr(inv_vp_matrix));

	glUniform1i(shader::uniform(shader_id, "fog_disabled"), flags & DEBUG_NOFOG);
	glUniform1i(shader::uniform(shader_id, "fog_nojitter"), flags & DEBUG_NOJITTER);
	glUniform1f(shader::uniform(shader_id, "fog_density"), fog_density);

	const glm::vec3& direction = current_scene->directional_light->direction;
	glUniform3f(shader::uniform(shader_id, "light_direction"), direction.x, direction.y, direction.z);

	ssquad->draw();
}

void Renderer::debug_gbuffer() {
	gbuffer->bind_for_light_pass();
	GLuint shader_id = shader::use("debug");
	glUniform1i(shader::uniform(shader_id, "tex_norm"), 1);
	glUniform1i(shader::uniform(shader_id, "tex_col"), 2);
	glUniform1i(shader::uniform(shader_id, "tex_depth"), 3);
	glUniform2f(shader::uniform(shader_id, "screensize"), gbuffer_width, gbuffer_height);
	
	ssquad->draw();
}

void Renderer::render(Scene* scene) {
	current_scene = scene;

	if (shadow != nullptr) {
		glViewport(0, 0, shadow->shadowmap.get_width(), shadow->shadowmap.get_height());
		shadow->shadowmap.bind_for_writing();
		render_geometry(shadow->view_matrix, shadow->proj_matrix);
		shadow->shadowmap.bind_for_reading();
		glViewport(0, 0, gbuffer_width, gbuffer_height);
	}

	gbuffer->start_frame();

	if (flags & DEBUG_WIREFRAME)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	gbuffer->bind_for_geom_pass();
	glm::mat4 view_matrix = current_scene->active_camera->view_matrix;
	glm::mat4 proj_matrix = current_scene->active_projection->proj_matrix;
	if (flags & DEBUG_LIGHTCAM) {
		view_matrix = shadow->view_matrix;
		proj_matrix = shadow->proj_matrix;
	}
	render_geometry(view_matrix, proj_matrix);
	
	if (flags & DEBUG_WIREFRAME)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	gbuffer->bind_for_light_pass();
	render_point_lights();

	if (current_scene->directional_light) {
		render_directional_light();
	}

	if (flags & DEBUG_GBUFFER) {
		debug_gbuffer();
	}

	gbuffer->bind_for_final_pass();
	render_final_pass();	

	current_scene = nullptr;
}

void Renderer::set_shadow(Shadow* shadow) {
	this->shadow = shadow;
}