#include "heightmap_material.hpp"

#include "shader.hpp"

HeightmapMaterial::HeightmapMaterial(Texture* height_texture, Texture* normal_texture, Texture* color_texture)
		: height_texture(height_texture), normal_texture(normal_texture), color_texture(color_texture) {
	static bool initialized = false;
	if (!initialized) {
		shader::create("heightmapmaterial", "heightmap.vs.glsl", GL_VERTEX_SHADER);
		shader::create("heightmapmaterial", "ds_geometry_textured.fs.glsl", GL_FRAGMENT_SHADER);
		initialized = true;
	}
	shader_id = shader::get("heightmapmaterial");
}

GLuint HeightmapMaterial::bind() {
	glUseProgram(shader_id);
	height_texture->bind_to_shader(shader::uniform(shader_id, "heightmap"));
	normal_texture->bind_to_shader(shader::uniform(shader_id, "normalmap"));
	color_texture->bind_to_shader(shader::uniform(shader_id, "colormap"));
	return shader_id;
}