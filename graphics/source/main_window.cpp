#include "main_window.hpp"

#include "config_parser.hpp"
#include "error.hpp"

#include <stdexcept>

MainWindow::MainWindow() : camera(nullptr), renderer(nullptr) {
	init_SDL();
	init_GL();

	active = true;
	SDL_ShowWindow(window);
}

MainWindow::~MainWindow() {
	cleanup_GL();
	cleanup_SDL();
}

void MainWindow::init_SDL() {
	// initialize SDL
	int ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret != 0) throw std::runtime_error(SDL_GetError());

	// initialize SDL_image
	IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);

	// read config
	std::string name = Config::get<std::string>("name");
	window_width = Config::get<int>("window width");
	window_height = Config::get<int>("window height");
	bool fullscreen = Config::get<int>("fullscreen");

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	// create Window
	uint window_flags = SDL_WINDOW_HIDDEN | SDL_WINDOW_OPENGL 
		| (fullscreen * SDL_WINDOW_FULLSCREEN);
	window = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
		window_width, window_height, window_flags);
	if (window == nullptr) throw std::runtime_error(SDL_GetError());

	// initialize GL context
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	gl_context = SDL_GL_CreateContext(window);

	if (!Config::get<int>("vsync")) {
		SDL_GL_SetSwapInterval(0);
	}

	SDL_SetRelativeMouseMode(SDL_TRUE);
}

void MainWindow::init_GL() {
	glewExperimental = GL_TRUE;
	glewInit();
	glGetError(); // ignore errors caused by glew

	if (glewIsSupported("GL_KHR_debug")) { 
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(Error::error_callback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_FALSE);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_HIGH, 0, nullptr, GL_TRUE);
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glClearColor(0.f, 0.f, 0.f, 0.f);
}

void MainWindow::cleanup_SDL() {
	if (gl_context != nullptr)
		SDL_GL_DeleteContext(gl_context);

	if (window != nullptr)
		SDL_DestroyWindow(window);

	IMG_Quit();
	SDL_Quit();
}

void MainWindow::cleanup_GL() {

}

void MainWindow::process_keydown(const SDL_Keycode& keycode) {
	switch (keycode) {
		case SDLK_1:
			if (renderer) renderer->flags ^= Renderer::DEBUG_GBUFFER;
			break;
		case SDLK_2:
			if (renderer) renderer->flags ^= Renderer::DEBUG_WIREFRAME;
			break;
		case SDLK_3:
			if (renderer) renderer->flags ^= Renderer::DEBUG_LIGHTCAM;
			break;
		case SDLK_4:
			if (renderer) renderer->flags ^= Renderer::DEBUG_NOFOG;
			break;
		case SDLK_5:
			if (renderer) renderer->flags ^= Renderer::DEBUG_NOJITTER;
			break;
		case SDLK_ESCAPE:
			active = false;
			break;
		default:
			if (camera)
				camera->key_pressed(keycode);
	}
}

void MainWindow::process_keyup(const SDL_Keycode& keycode) {
	switch (keycode) {
		default:
			if (camera)
				camera->key_released(keycode);
	}
}

void MainWindow::process_mousebutton(const SDL_MouseButtonEvent& event) {

}

void MainWindow::process_mousewheel(const SDL_MouseWheelEvent& event) {
	float fog_density = renderer->fog_density;
	if (fog_density > 0 && event.y == -1) fog_density -= 0.25;
	else if (fog_density < 7 && event.y == 1) fog_density += 0.25;
	renderer->fog_density = fog_density;
}

void MainWindow::process_mousemotion(const SDL_MouseMotionEvent& event) {
	if (SDL_GetRelativeMouseMode()) {
		// TODO: check if any buttons are pressed
		camera->mouse_moved(event.xrel, event.yrel);
	}
}

void MainWindow::process_windowevent(const SDL_WindowEvent& window_event) {
	switch(window_event.event) {
		case SDL_WINDOWEVENT_RESIZED:
			window_width = window_event.data1;
			window_height = window_event.data2;
			break;
	}
}

void MainWindow::process_events() {
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		switch(e.type) {
			case SDL_QUIT:
				active = false;
				break;
			case SDL_KEYDOWN:
				process_keydown(e.key.keysym.sym);
				break;
			case SDL_KEYUP:
				process_keyup(e.key.keysym.sym);
				break;
			case SDL_MOUSEBUTTONDOWN:
				process_mousebutton(e.button);
				break;
			case SDL_MOUSEMOTION:
				process_mousemotion(e.motion);
				break;
			case SDL_MOUSEWHEEL:
				process_mousewheel(e.wheel);
				break;
			case SDL_WINDOWEVENT:
				process_windowevent(e.window);
				break;
		}
	}
}

void MainWindow::attach_camera(Camera* camera) {
	this->camera = camera;
}

void MainWindow::attach_renderer(Renderer* renderer) {
	this->renderer = renderer;
}

void MainWindow::start_loop() {
	// glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MainWindow::end_loop() {
	SDL_GL_SwapWindow(window);
}
