#include "shadow.hpp"

Shadow::Shadow(uint width, uint height, DirectionalLight* light) : shadowmap(width, height) {
	proj_matrix = glm::ortho(-100.f, 100.f, -100.f, 100.f, -70.f, 250.f);
	// proj_matrix = glm::perspectiveFov<float>(90, width, height, 0.1, 1000);
	light_dir = light->direction;
}

void Shadow::update_matrices(Camera* current_cam) {
	// glm::vec3 pos(current_cam->pos.x, 30, current_cam->pos.z);
	glm::vec3 pos(40.f, 100.f, 60.f);
	view_matrix = glm::lookAt(pos, pos + light_dir, glm::vec3(0, 1, 0));
}