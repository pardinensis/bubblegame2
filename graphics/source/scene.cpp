#include "scene.hpp"


Scene::Scene() :
	active_camera(nullptr),
	active_projection(nullptr),
	directional_light(nullptr) {}

Scene::~Scene() {
}

void Scene::set_active_camera(Camera* cam) {
	active_camera = cam;
}

void Scene::set_active_projection(Projection* proj) {
	active_projection = proj;
}


void Scene::add_mesh(Mesh* mesh) {
	if (!meshes.insert(mesh).second)
		return;

	Material* mat = mesh->get_material();
	auto it = material_map.find(mat);
	if (it == material_map.end()) {
		material_map.emplace(mat, std::vector<Mesh*>{mesh});
	}
	else {
		it->second.push_back(mesh);
	}
}

void Scene::remove_mesh(Mesh* mesh) {
	auto it = meshes.find(mesh);
	if (it != meshes.end()) {
		meshes.erase(it);

		Material* mat = mesh->get_material();
		auto jt = material_map.find(mat);
		std::vector<Mesh*>& meshlist = jt->second;
		for (auto kt = meshlist.begin(); kt != meshlist.end(); ++kt) {
			if (*kt == mesh) {
				meshlist.erase(kt);
				break;
			}
		}
		if (meshlist.size() == 0) {
			material_map.erase(jt);
		}
	}
}

void Scene::set_directional_light(DirectionalLight* directional_light) {
	this->directional_light = directional_light;
}
