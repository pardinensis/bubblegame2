#include "projection.hpp"


Projection::Projection(const std::string& name, float width, float height, float nearplane, float farplane, float fovy) :
		name(name), width(width), height(height), nearplane(nearplane), farplane(farplane), fovy(fovy) {
	recalculate_matrix();
}

void Projection::recalculate_matrix() {
	proj_matrix = glm::perspectiveFov<float>(fovy, width, height, nearplane, farplane);
}