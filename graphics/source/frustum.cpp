#include "frustum.hpp"

void Plane::normalize() {
	p /= glm::length(n());
}

void Plane::set(glm::vec4 p) {
	this->p = p;
	normalize();
}

Frustum::Frustum(const glm::mat4& VP) {
	glm::vec4 r0 = glm::row(VP, 0);
	glm::vec4 r1 = glm::row(VP, 1);
	glm::vec4 r2 = glm::row(VP, 2);
	glm::vec4 r3 = glm::row(VP, 3);

	planes[0].set(r3 + r0); // left
	planes[1].set(r3 - r0); // right
	planes[2].set(r3 + r1); // bottom
	planes[3].set(r3 - r1); // top
	planes[4].set(r3 + r2); // near
	planes[5].set(r3 - r2); // far
}

bool Frustum::check_sphere(const glm::vec3& center, float radius) const {
	for (int i = 0; i < 6; ++i) {
		float dist = glm::dot(planes[i].n(), center) + planes[i].d();
		if (dist < -radius) {
			return false;
		}
	}
	return true;
}