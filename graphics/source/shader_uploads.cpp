#include "shader_uploads.hpp"

void shader::upload_model_matrix(GLuint prog, const glm::mat4& model_matrix) {
	glUniformMatrix4fv(shader::uniform(prog, "model_matrix"), 1, GL_FALSE, glm::value_ptr(model_matrix));
}

void shader::upload_model_and_normal_matrix(GLuint prog, const glm::mat4& model_matrix) {
	upload_model_matrix(prog, model_matrix);
	glm::mat4 normal_matrix = glm::transpose(glm::inverse(model_matrix));
	glUniformMatrix4fv(shader::uniform(prog, "normal_matrix"), 1, GL_FALSE, glm::value_ptr(normal_matrix));
}

void shader::upload_view_matrix(GLuint prog, const Camera& cam) {
	glUniformMatrix4fv(shader::uniform(prog, "view_matrix"), 1, GL_FALSE, glm::value_ptr(cam.view_matrix));
}

void shader::upload_proj_matrix(GLuint prog, const Projection& proj) {
	glUniformMatrix4fv(shader::uniform(prog, "proj_matrix"), 1, GL_FALSE, glm::value_ptr(proj.proj_matrix));
}

void shader::upload_identity_view_matrix(GLuint prog) {
	glUniformMatrix4fv(shader::uniform(prog, "view_matrix"), 1, GL_FALSE, glm::value_ptr(glm::mat4()));
}

void shader::upload_identity_proj_matrix(GLuint prog) {
	glUniformMatrix4fv(shader::uniform(prog, "proj_matrix"), 1, GL_FALSE, glm::value_ptr(glm::mat4()));
}