#include "pixel.hpp"


Pixel::Pixel() : r(0), g(0), b(0), a(0) {}
Pixel::Pixel(uchar r, uchar g, uchar b, uchar a) : r(r), g(g), b(b), a(a) {}
Pixel::Pixel(float r, float g, float b, float a) : r(r * 255.f), g(g * 255.f), b(b * 255.f), a(a * 255.f) {}

std::ostream& operator<<(std::ostream& os, const Pixel& p) {
	os << "(" << (uint)p.r << ", " << (uint)p.g << ", " << (uint)p.b << ", " << (uint)p.a << ")";
	return os;
}


float Pixel::get_r() const {
	return r / 255.f;
}

float Pixel::get_g() const {
	return g / 255.f;
}

float Pixel::get_b() const {
	return b / 255.f;
}

float Pixel::get_a() const {
	return a / 255.f;
}

glm::vec3 Pixel::get_rgb() const {
	return glm::vec3(r / 255.f, g / 255.f, b / 255.f);
}

glm::vec4 Pixel::get_rgba() const {
	return glm::vec4(r / 255.f, g / 255.f, b / 255.f, a / 255.f);
}

void Pixel::set_r(float r) {
	this->r = r * 255.f;
}

void Pixel::set_g(float g) {
	this->g = g * 255.f;
}

void Pixel::set_b(float b) {
	this->b = b * 255.f;
}

void Pixel::set_a(float a) {
	this->a = a * 255.f;
}

void Pixel::set_rgb(const glm::vec3& rgb) {
	r = uchar(rgb.x * 255.f);
	g = uchar(rgb.y * 255.f);
	b = uchar(rgb.z * 255.f);
}

void Pixel::set_rgba(const glm::vec4& rgba) {
	r = uchar(rgba.x * 255.f);
	g = uchar(rgba.y * 255.f);
	b = uchar(rgba.z * 255.f);
	a = uchar(rgba.w * 255.f);
}