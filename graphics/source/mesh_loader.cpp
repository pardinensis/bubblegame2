#include "mesh_loader.hpp"

#include "vao.hpp"

#include <utility>


std::vector<VAO*> MeshLoader::load_vaos(const std::string& file) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(file, aiProcess_Triangulate | aiProcess_GenSmoothNormals);

	if (!scene) {
		ERROR(std::string("could not read mesh file") + file);
	}

	uint n_vaos = scene->mNumMeshes;
	std::vector<VAO*> vaos;

	for (uint i = 0; i < n_vaos; ++i) {
		const aiMesh* mesh = scene->mMeshes[i];
		uint n_vertices = mesh->mNumVertices;
		uint n_triangles = mesh->mNumFaces;

		uint has_positions = mesh->HasPositions()      * VAO::BUFFER_CONTENT_TYPE_POSITION;
		uint has_normals   = mesh->HasNormals()        * VAO::BUFFER_CONTENT_TYPE_NORMAL;
		uint has_texcoords = mesh->HasTextureCoords(0) * VAO::BUFFER_CONTENT_TYPE_TEXCOORD;
		VAO* vao = new VAO(has_positions | has_normals | has_texcoords);
		vao->bind();

		if (has_positions) {
			float pos[3 * n_vertices];
			for (uint i = 0; i < n_vertices; ++i) {
				pos[3 * i + 0] = mesh->mVertices[i].x;
				pos[3 * i + 1] = mesh->mVertices[i].y;
				pos[3 * i + 2] = mesh->mVertices[i].z;
			}
			vao->change_vertex_data(VAO::BUFFER_CONTENT_TYPE_POSITION,
				GL_FLOAT, n_vertices, 3, pos);
		}

		if (has_normals) {
			vao->change_vertex_data(VAO::BUFFER_CONTENT_TYPE_NORMAL,
				GL_FLOAT, n_vertices, 3, (void*)(mesh->mNormals));
		}

		if (has_texcoords) {
			vao->change_vertex_data(VAO::BUFFER_CONTENT_TYPE_TEXCOORD,
				GL_FLOAT, n_vertices, 2, (void*)(mesh->mTextureCoords[0]));
		}

		std::vector<glm::ivec3> indices;
		for (uint j = 0; j < n_triangles; ++j) {
			const aiFace& face = mesh->mFaces[j];
			indices.push_back(glm::ivec3(face.mIndices[0], face.mIndices[1], face.mIndices[2]));
		}
		vao->change_index_data(3 * indices.size(), GL_TRIANGLES, indices.data());

		vao->unbind();
		vaos.push_back(vao);
	}

	return vaos;
}