#include "gbuffer.hpp"

GBuffer::GBuffer(uint window_width, uint window_height) {
    // Create the FBO
    glGenFramebuffers(1, &fbo_id);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);

    // Create the gbuffer textures
    glGenTextures(GBUFFER_N_TEXTURES, textures);
	glGenTextures(1, &depth_texture);
	glGenTextures(1, &final_texture);
    
    for (unsigned int i = 0 ; i < GBUFFER_N_TEXTURES; i++) {
    	glBindTexture(GL_TEXTURE_2D, textures[i]);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, window_width, window_height, 0, GL_RGB, GL_FLOAT, NULL);
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, textures[i], 0);
    }

	// depth (stencil)
	glBindTexture(GL_TEXTURE_2D, depth_texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH32F_STENCIL8, window_width, window_height, 0, GL_DEPTH_STENCIL, GL_FLOAT_32_UNSIGNED_INT_24_8_REV, NULL);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depth_texture, 0);

	// final
	glBindTexture(GL_TEXTURE_2D, final_texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, window_width, window_height, 0, GL_RGB, GL_FLOAT, NULL);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, final_texture, 0);

    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (Status != GL_FRAMEBUFFER_COMPLETE) {
        printf("FB error, status: 0x%x\n", Status);
        return;
    }

	// restore default FBO
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

GBuffer::~GBuffer() {
	glDeleteTextures(1, &final_texture);
	glDeleteTextures(1, &depth_texture);
	glDeleteTextures(GBUFFER_N_TEXTURES, textures);
    glDeleteFramebuffers(1, &fbo_id);    
    
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void GBuffer::start_frame() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_id);
	glDrawBuffer(GL_COLOR_ATTACHMENT0 + TEXTURE_FINAL);
	glClear(GL_COLOR_BUFFER_BIT);
}

void GBuffer::bind_for_geom_pass() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_id);
	GLenum draw_buffers[] = {
		GL_COLOR_ATTACHMENT0 + GBUFFER_TEXTURE_POS,
		GL_COLOR_ATTACHMENT0 + GBUFFER_TEXTURE_NORM,
		GL_COLOR_ATTACHMENT0 + GBUFFER_TEXTURE_COL
	};

	glDrawBuffers(3, draw_buffers);
}

void GBuffer::bind_for_stencil_pass() {
	glDrawBuffer(GL_NONE);
}

void GBuffer::bind_for_light_pass() {
	glDrawBuffer(GL_COLOR_ATTACHMENT0 + TEXTURE_FINAL);

	for (unsigned int i = 0; i < GBUFFER_N_TEXTURES; ++i) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, textures[i]);
	}

	glActiveTexture(GL_TEXTURE0 + TEXTURE_DEPTH_STENCIL);
	glBindTexture(GL_TEXTURE_2D, depth_texture);
}

void GBuffer::bind_for_final_pass() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	glActiveTexture(GL_TEXTURE0 + TEXTURE_DEPTH_STENCIL);
	glBindTexture(GL_TEXTURE_2D, depth_texture);
	glActiveTexture(GL_TEXTURE0 + TEXTURE_FINAL);
	glBindTexture(GL_TEXTURE_2D, final_texture);
}