#include "texture.hpp"

#include "common_includes.hpp"
#include "config_parser.hpp"

#include <map>



Texture::Texture(const std::string& imgfile) : mipmapping(false), texture_unit(0) {
	std::string imgdirectory = Config::get<std::string>("texture dir");
	std::string imgfilepath = imgdirectory + "/" + imgfile;
	surface = IMG_Load(imgfilepath.c_str());
	CHECK_SDL_ERROR(surface, "cannot find texture '" + imgfile + "'");

	width = surface->w;
	height = surface->h;

	SDL_LockSurface(surface);
	uchar* pixel_data = static_cast<uchar*>(surface->pixels);
	SDL_PixelFormat* fmt = surface->format;
	
	pixels.resize(width * height);
	if (fmt->palette == nullptr) {
		for (uint i = 0; i < width * height; ++i) {
			uint idx = (i / width) * surface->pitch + (i % width) * fmt->BytesPerPixel;
			uint pixel = *(reinterpret_cast<uint*>(pixel_data + idx));
			uchar r = ((pixel & fmt->Rmask) >> fmt->Rshift) << fmt->Rloss;
			uchar g = ((pixel & fmt->Gmask) >> fmt->Gshift) << fmt->Gloss;
			uchar b = ((pixel & fmt->Bmask) >> fmt->Bshift) << fmt->Bloss;
			uchar a = ((pixel & fmt->Amask) >> fmt->Ashift) << fmt->Aloss;
			pixels[i] = Pixel(r, g, b, a);
		}
	}
	else {
		for (uint i = 0; i < width * height; ++i) {
			uint idx = (i / width) * surface->pitch + (i % width) * fmt->BytesPerPixel;
			SDL_Color color = fmt->palette->colors[pixel_data[idx]];
			pixels[i] = Pixel(color.r, color.g, color.b, color.a);
		}
	}


	SDL_UnlockSurface(surface);

	glGenTextures(1, &id);
}

Texture::Texture(uint width, uint height) : surface(nullptr), width(width), height(height),
		mipmapping(false), texture_unit(0) {
	pixels = std::vector<Pixel>();
	pixels.resize(width * height);
	glGenTextures(1, &id);
}

Texture::~Texture() {
	glDeleteTextures(1, &id);

	if (surface != nullptr)
		SDL_FreeSurface(surface);
}

uint Texture::get_width() const {
	return width;
}

uint Texture::get_height() const {
	return height;
}

Pixel Texture::get_pixel(uint x, uint y) const {
	if (x >= width || y >= height)
		ERROR("Texture::get(): pixel coords not in range");

	return pixels[y * width + x];
}

void Texture::set_pixel(uint x, uint y, Pixel p) {
	if (x >= width || y >= height)
		ERROR("Texture::get(): pixel coords not in range");

	pixels[y * width + x] = p;
}

void Texture::set_mipmapping(bool mipmapping) {
	this->mipmapping = mipmapping;
}

void Texture::set_texture_unit(uint texture_unit) {
	this->texture_unit = texture_unit;
}

void Texture::upload() {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
	
	if (mipmapping) {
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	} else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void Texture::bind_to_shader(GLuint texture_loc) {
	glActiveTexture(GL_TEXTURE0 + texture_unit);
	glBindTexture(GL_TEXTURE_2D, id);
	glUniform1i(texture_loc, texture_unit);
}


/*

Texture::Texture() : width(0), height(0), n_channels(0),
	n_bytes_per_channel(0), mipmapping(false), id(0) {}

void Texture::load_from_file(const std::string& imgfile) {
	// read image file
	std::string imgdirectory = Config::get<std::string>("texture dir");
	std::string imgfilepath = imgdirectory + "/" + imgfile;
	SDL_Surface* surface = IMG_Load(imgfilepath.c_str());
	CHECK_SDL_ERROR(surface, "cannot find texture '" + imgfile + "'");

	// set attributes
	width = surface->w;
	height = surface->h;

	for (uint y = 0; y < height; ++y) {
		for (uint x = 0; x < width; ++x) {
			cpu_data.push_back(Pixel(surface, x, y));
		}
	}

	SDL_FreeSurface(surface);
}

Pixel Texture::get_pixel(uint x, uint y) const {
	return cpu_data[x + width * y];
}

void Texture::set_pixel(uint x, uint y, Pixel pixel) {
	cpu_data[x + width * y] = pixel;
}

void Texture::generate_gpu_texture() {
	if (id != 0) {
		std::cerr << id << std::endl;
		ERROR("gpu texture already created");
	}

	glGenTextures(1, &id);
}

void Texture::destroy_gpu_texture() {
	if (id != 0) {
		glDeleteTextures(1, &id);
		id = 0;
	}
}

void Texture::upload_gpu_data() {
	GLenum internal_format = 0;
	switch (n_channels) {
		case 1: internal_format = GL_RED; break;
		case 2: internal_format = GL_RG; break;
		case 3: internal_format = GL_RGB; break;
		case 4: internal_format = GL_RGBA; break;
		default: ERROR("invalid number of channels: " + std::to_string(n_channels));
	}

	GLenum type = 0;
	switch (n_bytes_per_channel) {
		case 1: type = GL_UNSIGNED_BYTE; break;
		case 2: type = GL_UNSIGNED_SHORT; break;
		case 4: type = GL_UNSIGNED_INT; break;
		default: ERROR("invalid number of bytes per channel: " + std::to_string(n_bytes_per_channel));
	}

	uint8_t* pixels = nullptr;
	uint n_pixels = cpu_data.size();
	if (n_pixels > 0) {
		uint n_bytes_per_pixel = 4 * n_bytes_per_channel;
		pixels = new uint8_t[n_pixels * n_bytes_per_pixel];
		for (uint i = 0; i < n_pixels; ++i) {
			cpu_data[i].store_to(pixels + i * n_bytes_per_pixel, n_bytes_per_channel);
		}
	}

	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, GL_RGBA, type, pixels);
	
	if (mipmapping) {
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	} else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

uint Texture::bind_to_texture_unit(uint texture_unit) {
	glActiveTexture(GL_TEXTURE0 + texture_unit);
	glBindTexture(GL_TEXTURE_2D, id);
	return texture_unit;
}

TextureRGB::TextureRGB(const std::string& imgfile) {
	load_from_file(imgfile);
	set_n_channels(3);
	set_n_bytes_per_channel(1);
	enable_mipmapping();
	generate_gpu_texture();
	upload_gpu_data();
}*/