#include "debug_camera.hpp"

#include "io.hpp"

DebugCamera::DebugCamera(const std::string& name)
	: DebugCamera(name, glm::vec3(), glm::vec3(), glm::vec3()) {}

DebugCamera::DebugCamera(const std::string& name, const glm::vec3& pos, const glm::vec3& dir, const glm::vec3& up)
	: Camera(name, pos, dir, up), slow_speed(10), fast_speed(50), rot_speed(0.0015),
	  key_w(false), key_a(false), key_s(false), key_d(false), key_lctrl(false) {}


void DebugCamera::key_pressed(const SDL_Keycode& keycode) {
	switch(keycode) {
		case SDLK_w: key_w = true; break;
		case SDLK_a: key_a = true; break;
		case SDLK_s: key_s = true; break;
		case SDLK_d: key_d = true; break;
		case SDLK_LCTRL: key_lctrl = true; break;
	}
}

void DebugCamera::key_released(const SDL_Keycode& keycode) {
	switch(keycode) {
		case SDLK_w: key_w = false; break;
		case SDLK_a: key_a = false; break;
		case SDLK_s: key_s = false; break;
		case SDLK_d: key_d = false; break;
		case SDLK_LCTRL: key_lctrl = false; break;
	}
}

void DebugCamera::mouse_moved(int xrel, int yrel) {
	glm::vec3 right = glm::normalize(glm::cross(dir, up));
	glm::quat quat_yaw = glm::angleAxis(-rot_speed * xrel, up);

	float newpitch = pitch - rot_speed * yrel;
	if (newpitch > M_PI/2 - 0.01)
		newpitch = M_PI/2 - 0.01;
	if (newpitch < -M_PI/2 + 0.01)
		newpitch = -M_PI/2 + 0.01;

	glm::quat quat_pitch = glm::angleAxis(newpitch - pitch, right);
	glm::quat rotation = quat_yaw * quat_pitch;
	dir = rotation * dir;

	pitch = newpitch;
}

void DebugCamera::update(float time_interval) {
	glm::vec3 right = glm::normalize(glm::cross(dir, up));
	float speed = (key_lctrl) ? fast_speed : slow_speed;
	float forward_speed = (int(key_w) - int(key_s)) * speed * time_interval;
	float sideward_speed = (int(key_d) - int(key_a)) * speed * time_interval;

	pos += forward_speed * dir;
	pos += sideward_speed * right;
	recalculate_matrix();
}