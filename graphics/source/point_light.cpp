#include "point_light.hpp"

#include "shader.hpp"

static uint loc_color;
static uint loc_intensity;
static uint loc_position;
static uint loc_exponential_falloff;

static const std::string vertex_shader_file = "ds_point.vs.glsl";
static const std::string fragment_shader_file = "ds_point.fs.glsl";
static uint shader_id = -1;


PointLight::PointLight(const glm::vec3& color, const glm::vec3& position, float intensity, float size)
	: color(color), position(position), intensity(intensity), size(size), exponential_falloff(5.545f/size) {}

void PointLight::init_shader() {
	shader::create("pointlight", vertex_shader_file, GL_VERTEX_SHADER);
	shader::create("pointlight", fragment_shader_file, GL_FRAGMENT_SHADER);
	shader_id = shader::get("pointlight");

	loc_color = shader::uniform(shader_id, "light_color");
	loc_position = shader::uniform(shader_id, "light_position");
	loc_intensity = shader::uniform(shader_id, "light_intensity");
	loc_exponential_falloff = shader::uniform(shader_id, "light_exponential_falloff");
}

uint PointLight::get_shader() {
	return shader_id;
}

uint PointLight::bind_shader() {
	glUseProgram(shader_id);
	return shader_id;
}

void PointLight::upload() {
	if (shader_id == (uint)-1)
		ERROR("directional light shader has not been initialized");

	glUniform3f(loc_color, color.x, color.y, color.z);
	glUniform3f(loc_position, position.x, position.y, position.z);
	glUniform1f(loc_intensity, intensity);
	glUniform1f(loc_exponential_falloff, exponential_falloff);
}