#version 330 core

in vec2 var_tc;

uniform sampler2D tex_shaded;
uniform sampler2D tex_depth;
uniform sampler2D tex_shadow;
uniform vec2 shadowmapsize;

uniform mat4 inv_vp_matrix;
uniform mat4 light_matrix;
uniform vec3 light_direction;

uniform float near;
uniform float far;
uniform vec2 screensize;

uniform float fog_density;
uniform bool fog_disabled;
uniform bool fog_nojitter;

uniform vec3 sky_color;

out vec4 out_col;


float shadow(vec3 wpos) {
	vec4 lpos = light_matrix * vec4(wpos, 1);
	vec3 uvz = 0.5 * (lpos.xyz / lpos.w) + vec3(0.5);

	if (uvz.x < 0 || uvz.x > 1 || uvz.y < 0 || uvz.y > 1)
		return 1.0;

	float px = 1.0 / shadowmapsize.x;
	float py = 1.0 / shadowmapsize.y;

	float light_factor = 0.3;
	float light_addend = (1.0 - light_factor);
	if (texture(tex_shadow, uvz.xy + vec2( px, py)).x > uvz.z - 0.001) light_factor += light_addend;
	return light_factor;
}

float lindepth(float d) {
	return (2 * near) / (far + near - d * (far - near));
}

float expdepth(float d) {
	return (far + near - 2 * near / d) / (far - near);
}

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float dist_to_alpha(float dist) {
	// return 1 - pow(2, -dist * fog_density);
	return dist * fog_density;
}

vec3 cpos2wpos(vec2 tc11, float depth11) {
	vec4 wpos_hom = inv_vp_matrix * vec4(tc11, depth11, 1);
	return wpos_hom.xyz / wpos_hom.w;
}

float bsdf(float cosphi) {
	// return 0.12; // lobmeier bsdf
	return 0.1875 / 3.14159 * (1 + cosphi * cosphi); // rayleigh bsdf
	// return 0.25 / 3.14159 * (0.5 + 4.5 * pow(0.5 * (1 + cosphi), 8)); // mie-hazy bsdf
	// return 0.25 / 3.14159 * (0.5 + 16.5 * pow(0.5 * (1 + cosphi), 32)); // mie-murky bsdf
}

void main() {
	vec2 tc = gl_FragCoord.xy / screensize;
	vec3 raw = texture(tex_shaded, tc).rgb;

	float depth = lindepth(texture(tex_depth, tc).r);

	vec2 tc11 = tc * 2 - vec2(1, 1);

	vec3 col = raw;
	vec3 fogcolor = vec3(1, 1, 1);

	float in_scatter_depth = 0;
	float out_scatter_depth = depth;

	float n_iterations = 32;
	float step_depth = 1 / n_iterations;
	float jitter = (fog_nojitter) ? 0 : rand(tc) * step_depth;
	for (float d = jitter; d < depth; d += step_depth) {
		float in_scatter = 0;
		if (d + step_depth < depth) {
			in_scatter = step_depth;
		} else {
			in_scatter = depth - d;
		}

		float depth11 = expdepth(d) * 2 - 1;
		vec3 wpos = cpos2wpos(tc11, depth11);
		if (fog_disabled)
			in_scatter_depth += in_scatter;
		else
			in_scatter_depth += in_scatter * shadow(wpos);
	}

	vec3 cam_pos = cpos2wpos(vec2(0, 0), 0);
	vec3 view_dir = normalize(cpos2wpos(tc11, 1) - cam_pos);
	float cosphi = dot(-view_dir, normalize(light_direction));
	float bsdf_factor = bsdf(cosphi);

	float in_scatter_alpha = max(0, dist_to_alpha(in_scatter_depth) * bsdf_factor);
	float out_scatter_alpha = min(1, dist_to_alpha(out_scatter_depth) * bsdf_factor);
	col = (1 - out_scatter_alpha) * col + in_scatter_alpha * fogcolor;

	out_col = vec4(col, 1);
}