#version 330 core

layout(location = 0) in vec3 pos;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 proj_matrix;

void main() {
	vec4 wpos = model_matrix * vec4(pos, 1);
	vec4 epos = view_matrix * wpos;
	vec4 spos = proj_matrix * epos;

	gl_Position = spos;
}