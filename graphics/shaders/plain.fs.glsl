#version 330 core

in vec4 wpos;
in vec3 wnorm;

out vec4 out_col;

void main() {
	vec3 col = wnorm * 0.00001 + vec3(1, 0, 1);
	out_col = vec4(col, 1);
}