#version 330 core

in vec4 wpos;
in vec3 wnorm;
in vec2 tc_color;

uniform sampler2D colormap;

layout (location = 0) out vec3 out_wpos;
layout (location = 1) out vec3 out_wnorm;
layout (location = 2) out vec3 out_col;


void main() {
	out_wpos = wpos.xyz;
	out_wnorm = wnorm;
	out_col = texture(colormap, tc_color).rgb;
}