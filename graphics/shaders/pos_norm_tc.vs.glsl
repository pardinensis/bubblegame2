#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 norm;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 proj_matrix;
uniform mat4 normal_matrix;

out vec4 wpos;
out vec3 wnorm;
out vec2 tc_color;

void main() {
	wpos = model_matrix * vec4(pos, 1);
	wnorm = (normal_matrix * vec4(norm, 1)).xyz;
	tc_color = wnorm.xz;

	vec4 epos = view_matrix * wpos;
	vec4 spos = proj_matrix * epos;

	
	gl_Position = spos;
}