#version 330 core

in vec4 wpos;
in vec3 wnorm;

uniform vec3 color;

layout (location = 0) out vec3 out_wpos;
layout (location = 1) out vec3 out_wnorm;
layout (location = 2) out vec3 out_col;


void main() {
	out_wpos = wpos.xyz;
	out_wnorm = wnorm;
	out_col = color;
}