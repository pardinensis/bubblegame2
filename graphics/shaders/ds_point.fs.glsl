#version 330 core

in vec2 var_tc;

uniform sampler2D tex_pos;
uniform sampler2D tex_norm;
uniform sampler2D tex_col;
uniform sampler2D tex_depth;

uniform vec2 screensize;

uniform vec3  light_color;
uniform float light_intensity;
uniform vec3 light_position;
uniform float light_exponential_falloff;

out vec4 out_col;


void main() {
	vec2 tc = gl_FragCoord.xy / screensize;
	vec3 wpos = texture(tex_pos, tc).xyz;
	vec3 wnorm = texture(tex_norm, tc).xyz;
	vec3 wcol = texture(tex_col, tc).xyz;
	float wdepth = texture(tex_depth, tc).x;

	vec3 diffuse_term = vec3(0, 0, 0);
	vec3 light_direction = normalize(wpos - light_position);
	float ndotl = dot(-light_direction, wnorm);
	if (wdepth < 1 && ndotl > 0) {
		float dist = distance(wpos, light_position);
		float falloff = exp(- light_exponential_falloff * dist);
		diffuse_term = ndotl * light_color * light_intensity * falloff;
	}

	vec3 light_term = diffuse_term;
	out_col = vec4(light_term * wcol, 1);
}