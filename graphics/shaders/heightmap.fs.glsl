#version 330 core

in vec4 wpos;
in vec3 wnorm;

uniform vec3 wlight_pos;

out vec4 out_col;

void main() {

	out_col = vec4(0.5 * wnorm + vec3(0.5), 1);
}