#version 330 core

in vec4 wpos;
in vec3 wnorm;
in vec2 tc;

uniform vec4 wlight_pos;
uniform float tc_scale;
uniform sampler2D colormap;

out vec4 out_col;

void main() {
	vec3 fullbright_col = texture(colormap, tc * tc_scale).rgb;
	vec3 ambient_col = 0.1 * fullbright_col;

	vec3 wlight_dir = normalize(wlight_pos.xyz / wlight_pos.w - wpos.xyz);
	float ndotl = max(0, dot(wnorm, wlight_dir));
	vec3 diffuse_col = ndotl * fullbright_col;

	out_col = vec4(ambient_col + diffuse_col, 1);
}