#version 330 core


uniform sampler2D tex_pos;
uniform sampler2D tex_norm;
uniform sampler2D tex_col;
uniform sampler2D tex_depth;
uniform vec2 screensize;

out vec4 out_col;

void main() {
	vec2 tc = gl_FragCoord.xy / screensize;

	out_col = vec4(0, 0, 0, 0);
	if (tc.x < 0.5 && tc.y >= 0.5) {
		out_col = vec4(texture(tex_col, 2 * tc).rgb, 1);
	}
	else if (tc.x >= 0.5 && tc.y >= 0.5) {
		out_col = vec4(texture(tex_norm, 2 * tc).rgb, 1);
	}
	else if (tc.x < 0.5 && tc.y < 0.5) {
		out_col = vec4(texture(tex_pos, 2 * tc).rgb, 1);
	}
	else if (tc.x >= 0.5 && tc.y < 0.5) {
		float f = 1000.0;
		float n = 0.1;
		float z = (2 * n) / (f + n - texture(tex_depth, 2 * tc).x * (f - n));
		out_col = vec4(z, z, z, 1);
	}
}