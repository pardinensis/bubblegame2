#version 330 core

layout(location = 0) in vec2 pos;

uniform mat4 model_matrix;
uniform mat4 normal_matrix;
uniform mat4 view_matrix;
uniform mat4 proj_matrix;

uniform sampler2D heightmap;
uniform sampler2D normalmap;
uniform vec2 offset;
uniform float size;

out vec4 wpos;
out vec3 wnorm;
out vec2 tc_color;


void main() {
	vec2 tc = (pos + offset + 1) / (size + 1);
	float height = texture(heightmap, tc).r;
	wpos = model_matrix * vec4((pos.x + offset.x) / (size - 1), height, (pos.y + offset.y) / (size - 1), 1);;
	vec4 epos = view_matrix * wpos;
	vec4 spos = proj_matrix * epos;

	vec3 normal01 = texture(normalmap, tc).rgb;
	vec3 normal11 = normal01 * 2 - vec3(1, 1, 1);
	wnorm = normalize((normal_matrix * vec4(normal11, 1)).xyz);

	tc_color = tc * 25;
	gl_Position = spos;
}