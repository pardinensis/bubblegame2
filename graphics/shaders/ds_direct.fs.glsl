#version 330 core

uniform sampler2D tex_pos;
uniform sampler2D tex_norm;
uniform sampler2D tex_col;
uniform sampler2D tex_depth;
uniform vec2 screensize;

uniform sampler2D tex_shadow;
uniform vec2 shadowmapsize;

uniform vec3  light_color;
uniform float light_intensity;
uniform vec3  light_direction;
uniform vec3  light_ambient_color;
uniform float light_ambient_intensity;

uniform vec3 sky_color;

uniform mat4  light_matrix;


out vec4 out_col;


float shadow(vec3 wpos) {
	vec4 lpos = light_matrix * vec4(wpos, 1);
	vec3 uvz = 0.5 * (lpos.xyz / lpos.w) + vec3(0.5);

	if (uvz.x < 0 || uvz.x > 1 || uvz.y < 0 || uvz.y > 1)
		return 1.0;

	float px = 1.0 / shadowmapsize.x;
	float py = 1.0 / shadowmapsize.y;

	float light_factor = 0.5;
	float light_addend = (1.0 - light_factor) / 4.0;
	if (texture(tex_shadow, uvz.xy + vec2(-px,-py)).x > uvz.z - 0.001) light_factor += light_addend;
	if (texture(tex_shadow, uvz.xy + vec2(-px, py)).x > uvz.z - 0.001) light_factor += light_addend;
	if (texture(tex_shadow, uvz.xy + vec2( px,-py)).x > uvz.z - 0.001) light_factor += light_addend;
	if (texture(tex_shadow, uvz.xy + vec2( px, py)).x > uvz.z - 0.001) light_factor += light_addend;
	return light_factor;
}

void main() {
	vec2 tc = gl_FragCoord.xy / screensize;
	vec3 wpos = texture(tex_pos, tc).xyz;
	vec3 wnorm = texture(tex_norm, tc).xyz;
	vec3 wcol = texture(tex_col, tc).xyz;
	float wdepth = texture(tex_depth, tc).x;

	vec3 light_term = vec3(0, 0, 0);

	if (wdepth < 1) {
		vec3 ambient_term = light_ambient_color * light_ambient_intensity;

		vec3 diffuse_term = vec3(0, 0, 0);
		float ndotl = dot(-light_direction, wnorm);
		if (ndotl > 0) {
			diffuse_term = ndotl * light_color * light_intensity * shadow(wpos);
		}

		vec3 light_term = ambient_term + diffuse_term;
		out_col = vec4(light_term * wcol, 1);
	}
	else {
		out_col = vec4(sky_color, 1);
	}


	// out_col = vec4(texture(tex_shadow, tc).x);
}