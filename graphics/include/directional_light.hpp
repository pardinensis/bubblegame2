#pragma once

#include "common_includes.hpp"

class DirectionalLight {
public:
	glm::vec3 color;
	glm::vec3 ambient_color;
	glm::vec3 direction;
	float intensity;
	float ambient_intensity;

public:
	DirectionalLight(const glm::vec3& color, const glm::vec3& ambient_color, const glm::vec3& direction,
		float intensity, float ambient_intensity);

	static void init_shader();
	static uint get_shader();
	static uint bind_shader();

	void upload();
};