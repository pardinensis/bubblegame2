#pragma once

#include <GL/glew.h>

#include <string>
#include <cstring>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define CHECK_GL_ERROR() Error::checkGL(__FILENAME__, __LINE__)
#define CHECK_SDL_ERROR(object, message) Error::checkSDL(object, message, __FILENAME__, __LINE__)
#define ERROR(message) Error::error(message, __FILENAME__, __LINE__)
#define WARNING(message) Error::warning(message, __FILENAME__, __LINE__)

namespace Error {
	void checkGL(const char* filename, int line);
	void checkSDL(void* object, const std::string& message, const char* filename, int line);
	void error(const std::string& message, const char* filename, int line);
	void error_callback(GLenum source, GLenum type, GLuint id, GLenum severity,
		GLsizei length, const GLchar *message, void *userParam);
	void warning(const std::string& message, const char* filename, int line);
}