#include "shader.hpp"
#include "camera.hpp"
#include "projection.hpp"

namespace shader {
	void upload_model_matrix(GLuint prog, const glm::mat4& model_matrix);
	void upload_model_and_normal_matrix(GLuint prog, const glm::mat4& model_matrix);
	void upload_view_matrix(GLuint prog, const Camera& cam);
	void upload_proj_matrix(GLuint prog, const Projection& proj);
	void upload_identity_view_matrix(GLuint prog);
	void upload_identity_proj_matrix(GLuint prog);
}