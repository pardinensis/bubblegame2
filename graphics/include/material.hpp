#pragma once

#include "common_includes.hpp"

class Material {
protected:
	GLuint shader_id;

public:
	virtual ~Material();

	virtual GLuint bind() = 0;
};