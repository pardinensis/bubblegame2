#pragma once

#include "common_includes.hpp"

class Projection {
public:
	std::string name;

	float width;
	float height;
	float nearplane;
	float farplane;
	float fovy;

	glm::mat4 proj_matrix;

public:
	Projection(const std::string& name, float width, float height, float nearplane, float farplane, float fovy);

	void recalculate_matrix();
};