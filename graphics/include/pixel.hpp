#pragma once

#include "common_includes.hpp"

class Pixel {
private:
	uchar r, g, b, a;

public:
	Pixel();
	Pixel(uchar r, uchar g, uchar b, uchar a);
	Pixel(float r, float g, float b, float a);

	
	float get_r() const;
	float get_g() const;
	float get_b() const;
	float get_a() const;
	glm::vec3 get_rgb() const;
	glm::vec4 get_rgba() const;

	void set_r(float r);
	void set_g(float g);
	void set_b(float b);
	void set_a(float a);
	void set_rgb(const glm::vec3& rgb);
	void set_rgba(const glm::vec4& rgba);

	friend std::ostream& operator<<(std::ostream& os, const Pixel& p);
} __attribute__((packed));

std::ostream& operator<<(std::ostream& os, const Pixel& p);
