#pragma once

#include "material.hpp"

class PhongMaterial : public Material {
private:
	glm::vec3 color;

public:
	PhongMaterial(const glm::vec3& color);

	virtual GLuint bind();
};