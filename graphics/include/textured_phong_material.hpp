#pragma once

#include "material.hpp"
#include "texture.hpp"

class TexturedPhongMaterial : public Material {
public:
	Texture texture;

public:
	TexturedPhongMaterial(const std::string& tex_file);

	virtual GLuint bind();
};