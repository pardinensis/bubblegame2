#pragma once

#include "camera.hpp"
#include "directional_light.hpp"
#include "mesh.hpp"
#include "point_light.hpp"
#include "projection.hpp"

#include <map>
#include <set>

class Scene {
	friend class Renderer;

private:
	// camera
	Camera* active_camera;
	Projection* active_projection;

	// geometry
	std::set<Mesh*> meshes;
	std::map<Material*, std::vector<Mesh*>> material_map;

	// lighting
	DirectionalLight* directional_light;
	std::vector<PointLight*> point_lights;

public:
	Scene();
	~Scene();

	void set_active_camera(Camera* cam);
	void set_active_projection(Projection* proj);

	void add_mesh(Mesh* mesh);
	void remove_mesh(Mesh* mesh);

	void set_directional_light(DirectionalLight* directional_light);
};