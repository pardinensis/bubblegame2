#pragma once

#include "material.hpp"
#include "texture.hpp"

class HeightmapMaterial : public Material {
private:
	Texture* height_texture;
	Texture* normal_texture;
	Texture* color_texture;

public:
	HeightmapMaterial(Texture* height_texture, Texture* normal_texture, Texture* color_texture);

	virtual GLuint bind();
};