#pragma once

#include "camera.hpp"

class DebugCamera : public Camera {
private:
	float slow_speed, fast_speed, rot_speed;
	bool key_w, key_a, key_s, key_d;
	bool key_lctrl;

	float pitch;

public:
	DebugCamera(const std::string& name);
	DebugCamera(const std::string& name, const glm::vec3& pos, const glm::vec3& dir, const glm::vec3& up);

	virtual void key_pressed(const SDL_Keycode& keycode);
	virtual void key_released(const SDL_Keycode& keycode);
	virtual void mouse_moved(int xrel, int yrel);

	virtual void update(float time_interval);
};