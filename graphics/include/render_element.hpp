#pragma once

#include "common_includes.hpp"
#include "mesh.hpp"
#include "texture.hpp"

typedef std::vector<std::shared_ptr<Mesh>> mesh_list;
typedef std::vector<std::shared_ptr<Texture>> texture_list;

class RenderElement {
private:
	std::string name;

	glm::mat4 model_matrix;
	glm::mat3 normal_matrix;

	GLuint shader_id;

	mesh_list meshes;
	texture_list textures;

public:
	RenderElement(const std::string name);

	void set_model_matrix(const glm::mat3& model_matrix);

	void add_mesh(std::shared_ptr<Mesh> mesh);
	void add_texture(std::shared_ptr<Texture> texture, int texture_unit = -1);

	virtual void bind_uniforms();
};