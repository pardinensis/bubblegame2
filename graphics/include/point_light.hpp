#pragma once

#include "common_includes.hpp"

class PointLight {
public:
	glm::vec3 color;
	glm::vec3 position;
	float intensity;
	float size;
	float exponential_falloff;

public:
	PointLight(const glm::vec3& color, const glm::vec3& position, float intensity, float size);

	static void init_shader();
	static uint get_shader();
	static uint bind_shader();

	void upload();
};