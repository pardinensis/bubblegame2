#pragma once

#include <memory>

#include "common_includes.hpp"
#include "timer.hpp"

class Camera {
private:
	Timer timer;

public:
	std::string name;

	glm::vec3 pos;
	glm::vec3 dir;
	glm::vec3 up;

	glm::mat4 view_matrix;

public:
	Camera(const std::string& name);
	Camera(const std::string& name, const glm::vec3& pos, const glm::vec3& dir, const glm::vec3& up);

	virtual void key_pressed(const SDL_Keycode& keycode);
	virtual void key_released(const SDL_Keycode& keycode);
	virtual void mouse_moved(int xrel, int yrel);

	virtual void update(float time_interval);
	void update_camera();

	void recalculate_matrix();
};