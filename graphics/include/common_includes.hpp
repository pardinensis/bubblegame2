#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

#include <GL/glew.h>

#define GL_GLEXT_PROTOTYPES 1
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#include <iostream>
#include <limits>
#include <memory>
#include <unordered_map>
#include <string>
#include <vector>

#include "error.hpp"

typedef unsigned int uint;
typedef unsigned char uchar;
