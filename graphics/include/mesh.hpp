#pragma once

#include "common_includes.hpp"
#include "material.hpp"
#include "vao.hpp"

class Mesh {
protected:
	std::vector<VAO*> vaos;
	Material* mat;

	glm::mat4 model_matrix;
	glm::mat4 normal_matrix;

public:
	Mesh(const std::vector<VAO*>& vaos = std::vector<VAO*>(), Material* mat = nullptr);
	Mesh(const std::string& vao_file, Material* mat = nullptr);
	virtual ~Mesh();

	void get_model_matrix(glm::mat4& model_matrix) const;
	void set_model_matrix(const glm::mat4& model_matrix);

	Material* get_material() const;
	void set_material(Material* material);

	virtual void draw(GLuint shader_id);
};