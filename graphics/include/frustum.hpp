#pragma once

#include "common_includes.hpp"

struct Plane {
	glm::vec4 p;

	glm::vec3 n() const {
		return glm::vec3(p.x, p.y, p.z);
	}

	float d() const {
		return p.w;
	}

	void normalize();
	void set(glm::vec4);
};

class Frustum {
private:
	Plane planes[6];

public:
	Frustum(const glm::mat4& VP);

	bool check_sphere(const glm::vec3& center, float radius) const;
};