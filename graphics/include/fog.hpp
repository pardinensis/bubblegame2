#pragma once

#include "common_includes.hpp"

class Fog {
public:
	static void init_shader();
	static uint get_shader();
	static uint bind_shader();
};