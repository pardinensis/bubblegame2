#pragma once

#include "common_includes.hpp"

class GBuffer {
public:
	const static uint GBUFFER_TEXTURE_POS  = 0;
	const static uint GBUFFER_TEXTURE_NORM = 1;
	const static uint GBUFFER_TEXTURE_COL  = 2;
	const static uint GBUFFER_N_TEXTURES   = 3;

	const static uint TEXTURE_DEPTH_STENCIL = 3;
	const static uint TEXTURE_FINAL         = 4;


private:
	GLuint fbo_id;
	GLuint textures[GBUFFER_N_TEXTURES];
	GLuint depth_texture;
	GLuint final_texture;

public:

	GBuffer(uint window_width, uint window_height);
	~GBuffer();

	void start_frame();
	void bind_for_geom_pass();
	void bind_for_stencil_pass();
	void bind_for_light_pass();
	void bind_for_final_pass();
};