#pragma once

#include "common_includes.hpp"
#include "mesh.hpp"

#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

namespace MeshLoader {
	std::vector<VAO*> load_vaos(const std::string& file);
}