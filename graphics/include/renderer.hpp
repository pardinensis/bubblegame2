#pragma once

#include "common_includes.hpp"

#include "gbuffer.hpp"
#include "scene.hpp"
#include "shadow.hpp"
#include "vbuffer.hpp"

class Renderer {
private:
	Scene* current_scene;

	uint gbuffer_width, gbuffer_height;
	GBuffer* gbuffer;
	VBuffer* vbuffer;

	VAO* ssquad;

	Shadow* shadow;

public:
	float fog_density;

public:
	uint flags;
	static const uint DEBUG_GBUFFER   =  1;
	static const uint DEBUG_WIREFRAME =  2;
	static const uint DEBUG_LIGHTCAM  =  4;
	static const uint DEBUG_NOFOG     =  8;
	static const uint DEBUG_NOJITTER  = 16;

private:
	void render_geometry(const glm::mat4& view_matrix, const glm::mat4& proj_matrix);
	void render_point_lights();
	void render_directional_light();
	void render_final_pass();

	void debug_gbuffer();

public:
	Renderer(uint width, uint height);
	~Renderer();

	void set_shadow(Shadow* shadow);

	void render(Scene* scene);
};