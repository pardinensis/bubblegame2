#pragma once

#include "common_includes.hpp"
#include "camera.hpp"
#include "directional_light.hpp"
#include "shadowmap.hpp"

class Shadow {
private:
	glm::vec3 light_dir;

public:
	ShadowMap shadowmap;
	glm::mat4 view_matrix;
	glm::mat4 proj_matrix;

public:
	Shadow(uint width, uint height, DirectionalLight* light);

	void update_matrices(Camera* current_cam);
};