#pragma once

#include "common_includes.hpp"

#include "camera.hpp"
#include "renderer.hpp"

class MainWindow {
private:
	uint window_width, window_height;
	SDL_Window* window;
	SDL_GLContext gl_context;

	Camera* camera;
	Renderer* renderer;

public:
	volatile bool active;

public:
	MainWindow();
	~MainWindow();

	void process_events();

	void start_loop();
	void end_loop();

	void attach_camera(Camera* camera);
	void attach_renderer(Renderer* renderer);

private:
	void init_SDL();
	void init_GL();

	void cleanup_SDL();
	void cleanup_GL();

	void process_keydown(const SDL_Keycode& keycode);
	void process_keyup(const SDL_Keycode& keycode);
	void process_mousebutton(const SDL_MouseButtonEvent& event);
	void process_mousemotion(const SDL_MouseMotionEvent& event);
	void process_mousewheel(const SDL_MouseWheelEvent& event);
	void process_windowevent(const SDL_WindowEvent& window_event);
};