#pragma once

#include "common_includes.hpp"
#include "frustum.hpp"
#include "mesh.hpp"
#include "texture.hpp"
#include "vao.hpp"


#define CHUNK_SIZE 16 // max number of triangles per edge

class HMNode {
public:
	HMNode* child_ll;
	HMNode* child_lh;
	HMNode* child_hl;
	HMNode* child_hh;
	glm::vec3 aabb_l, aabb_h; // bounding box (low, high)
	glm::vec3 bs_c; float bs_r; // bounding sphere (center, radius)

public:
	HMNode();
	virtual ~HMNode() {}

	virtual void calculate_bounding_box(float size, const glm::vec3& scale);
	virtual void calculate_bounding_sphere();
	virtual void draw(GLuint prog, const Frustum& frustum);
};

class HMChunk : public HMNode {
	friend class Heightmap;
private:
	VAO* vao;
	glm::vec2 offset;
	Texture* texture;

public:
	static void init_cpu_buffers();

	HMChunk(glm::vec2 offset, Texture* texture);

	virtual void calculate_bounding_box(float size, const glm::vec3& scale);
	virtual void calculate_bounding_sphere();
	virtual void draw(GLuint prog, const Frustum& frustum);
};


class Heightmap : public Mesh {
	Texture* height_texture;
	Texture* normal_texture;
	uint size; // n_vertices per dimension

	// heightmap chunks (CPU)
	std::vector<HMChunk> chunks;
	HMNode* quadtree;

private:
	HMNode* createQuadtree(uint level, uint idx_x, uint idx_y);
	void deleteQuadtree(HMNode* node, uint level);

	void smooth(Texture& raw, Texture& filtered);

public:
	Heightmap(const std::string& height_texture_file, Texture* color_texture, glm::vec3 scale);
	~Heightmap();

	virtual void draw(GLuint shader_id);
};