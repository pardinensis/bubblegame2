#pragma once

#include "common_includes.hpp"


class VAO {
public:
	enum {
		BUFFER_CONTENT_TYPE_POSITION = 1 << 0,
		BUFFER_CONTENT_TYPE_NORMAL   = 1 << 1,
		BUFFER_CONTENT_TYPE_TEXCOORD = 1 << 2,

		N_BUFFER_CONTENT_TYPES = 3
	};

private:
	GLuint vao_id;               // vertex array object id
	std::vector<GLuint> vbo_ids; // vertex buffers
	GLuint ib_id;                // index buffer

	GLenum primitive_type;

	GLuint n_vertices;
	GLuint n_indices;

	uint n_vertex_buffers;
	std::vector<int> buffer_locations;

private:
	uint buffer_content_to_location(uint buffer_content);

public:
	VAO(uint buffer_contents);
	~VAO();

	void change_vertex_data(uint buffer_content, GLenum type, uint n_vertices, uint n_dimensions, const void* data);
	void change_index_data(uint n_indices, GLenum primitive_type, const void* data);

	void bind();
	void unbind();
	
	void draw();
};