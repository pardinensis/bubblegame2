#pragma once

#include "common_includes.hpp"

class ShadowMap {
private:
	uint width;
	uint height;
	uint texture_unit;
	GLuint fbo_id;
	GLuint tex_id;

public:
	ShadowMap(uint width, uint height);
	~ShadowMap();

	uint get_texture_unit() const;
	void set_texture_unit(uint texture_unit);

	uint get_width() const;
	uint get_height() const;

	void bind_for_writing();
	void bind_for_reading();
	void bind_fbo();
};