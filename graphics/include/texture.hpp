#pragma once

#include "common_includes.hpp"
#include "pixel.hpp"


class Texture {
	SDL_Surface* surface;
	std::vector<Pixel> pixels;

	uint width;
	uint height;

	GLuint id;

	bool mipmapping;
	uint texture_unit;

public:
	Texture(const std::string& file);
	Texture(uint width, uint height);
	~Texture();

	uint get_width() const;
	uint get_height() const;

	Pixel get_pixel(uint x, uint y) const;
	void set_pixel(uint x, uint y, Pixel p);

	void set_mipmapping(bool mipmapping);
	void set_texture_unit(uint texture_unit);

	void upload();
	void bind_to_shader(GLuint texture_loc);
};


/*
class Texture {
protected:
	// CPU
	uint width, height;
	std::vector<Pixel> cpu_data;

	// GPU
	uint n_channels;
	uint n_bytes_per_channel;
	bool mipmapping;
	GLuint id;


public:
	Texture();
	Texture(const Texture& other) = delete;

	void load_from_file(const std::string& imgfile);
	
	void get_size(uint& w, uint& h) { w = width; h = height; }
	void set_size(uint w, uint h) { width = w; height = h; cpu_data.resize(width * height); }

	Pixel get_pixel(uint x, uint y) const;
	void set_pixel(uint x, uint y, Pixel pixel);
	
	void enable_mipmapping() { mipmapping = true; }
	void disable_mipmapping() { mipmapping = false; }

	uint get_n_channels() const { return n_channels; }
	void set_n_channels(uint n_channels) { this->n_channels = n_channels; }
	uint get_n_bytes_per_channel() const { return n_bytes_per_channel; }
	void set_n_bytes_per_channel(uint n_bytes_per_channel) { this->n_bytes_per_channel = n_bytes_per_channel; }
	GLuint get_id() const { return id; }

	void generate_gpu_texture();
	void destroy_gpu_texture();
	void upload_gpu_data();

	uint bind_to_texture_unit(uint texture_unit);
};

class TextureRGB : public Texture {
public:
	TextureRGB(const std::string& imgfile);
};*/