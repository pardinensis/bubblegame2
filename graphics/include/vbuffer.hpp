#pragma once

#include "common_includes.hpp"

class VBuffer {
private:
	GLuint fbo_id;
	GLuint vbuf_texture;

	uint tex_width, tex_height;
	uint depth_width, depth_height;
	uint width, height, depth;

public:
	VBuffer(uint width, uint height, uint depth);
	~VBuffer();

	void bind_for_reading();
	void bind_for_writing();
};