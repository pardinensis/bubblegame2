#include "common_includes.hpp"

class Buffer {
protected:
	GLuint id;
	GLuint texture_unit;

public:
	Buffer();

	GLuint create_buffer_rgb(uint width, uint height);

	inline GLuint get_id() const { return id; }
	inline GLuint get_texture_unit() const { return texture_unit; }
	inline void set_texture_unit(GLuint texture_unit) { this->texture_unit = texture_unit; }

	void bind(GLuint loc);
};