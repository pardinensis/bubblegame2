#include "balloon.hpp"

#include "common_includes.hpp"
#include "phong_material.hpp"
#include "textured_phong_material.hpp"

Balloon::Balloon()
	: Mesh("../graphics/models/balloon.obj",
		new PhongMaterial(glm::vec3(0.8, 0.3, 0.2))) {
		// new TexturedPhongMaterial("grass01.png")) {

	set_model_matrix(glm::translate(glm::vec3(70, 30, 70)));

	// TexturedPhongMaterial* tex_mat = dynamic_cast<TexturedPhongMaterial*>(mat);
}

Balloon::~Balloon() {
	delete mat;
}
