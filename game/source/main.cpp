// game
#include "balloon.hpp"
#include "box.hpp"
#include "cube.hpp"

// graphics
#include "debug_camera.hpp"
#include "heightmap.hpp"
#include "main_window.hpp"
#include "renderer.hpp"
#include "shadow.hpp"

// utils
#include "config_parser.hpp"
#include "fps.hpp"

int main() {
	Config::parse();
	uint width = Config::get<int>("window width");
	uint height = Config::get<int>("window height");
	uint shadow_width = Config::get<int>("shadow width");
	uint shadow_height = Config::get<int>("shadow height");

	MainWindow main_window;

	Renderer renderer(width, height);
	main_window.attach_renderer(&renderer);

	Scene scene;

	DebugCamera debug_camera("debug_camera", glm::vec3(120, 35, 120),
		glm::normalize(glm::vec3(-1, 0, -1.8)), glm::vec3(0, 1, 0));
	main_window.attach_camera(&debug_camera);
	scene.set_active_camera(&debug_camera);

	Projection projection("default_proj", width, height, 0.1, 200, 60 * M_PI/180);
	scene.set_active_projection(&projection);

	DirectionalLight sun(glm::vec3(0.93, 0.82, 0.58), glm::vec3(0.59, 0.73, 0.88),
		glm::normalize(glm::vec3(6, -9, 2)), 1.5, 0.3);
	scene.set_directional_light(&sun);

	Shadow shadow(shadow_width, shadow_height, &sun);
	renderer.set_shadow(&shadow);

	Balloon balloon;
	scene.add_mesh(&balloon);

	std::vector<Cube*> cubes;
	for (int i = 0; i < 4; ++i) {
		cubes.push_back(new Cube(i));
		scene.add_mesh(cubes[i]);
	}

	Box box;
	for (Mesh* m : box.meshes) {
		scene.add_mesh(m);
	}

	Texture grass_texture("grass01.png");
	Heightmap heightmap("heightmap129.png", &grass_texture, glm::vec3(150, 40, 150));
	scene.add_mesh(&heightmap);


	FPS fps(0.01);

	while(main_window.active) {
		main_window.process_events();
		main_window.start_loop();

		debug_camera.update_camera();
		shadow.update_matrices(&debug_camera);

		renderer.render(&scene);

		main_window.end_loop();

		fps.print(1);
	}

	return 0;
}