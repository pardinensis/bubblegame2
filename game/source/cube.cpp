#include "cube.hpp"

#include "common_includes.hpp"
#include "phong_material.hpp"

Cube::Cube(uint id)
	: Mesh("../graphics/models/cube.obj",
		new PhongMaterial(glm::vec3(0.2, 0.8, 0.4))) {
	if (id % 2) {
		glm::mat4 scale = glm::scale(glm::vec3(40, 5, 10));
		glm::mat4 trans = glm::translate(glm::vec3(20, 75, 10 + (id/2) * 24));
		set_model_matrix(trans * scale);
	}
	else {
		glm::mat4 scale = glm::scale(glm::vec3(5, 60, 10));
		glm::mat4 trans = glm::translate(glm::vec3(55, 15, 10 + (id/2) * 24));
		set_model_matrix(trans * scale);
	}
}

Cube::~Cube() {
	delete mat;
}
