#include "box.hpp"

#include "common_includes.hpp"
#include "phong_material.hpp"
#include "mesh_loader.hpp"

Box::Box() {
	mat = new PhongMaterial(glm::vec3(0.1, 0.1, 0.1));

	std::vector<VAO*> cube = MeshLoader::load_vaos("../graphics/models/cube.obj");

	Mesh* side1 = new Mesh(cube, mat);
	side1->set_model_matrix(glm::translate(glm::vec3(0, 0, -0.5)) * glm::scale(glm::vec3(150, 120, 1)));
	meshes.push_back(side1);

	Mesh* side2 = new Mesh(cube, mat);
	side2->set_model_matrix(glm::translate(glm::vec3(-0.5, 0, 0)) * glm::scale(glm::vec3(1, 120, 150)));
	meshes.push_back(side2);
}

Box::~Box() {
	delete mat;
	for (Mesh* m : meshes) {
		delete m;
	}
}
