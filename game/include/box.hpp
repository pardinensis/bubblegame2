#pragma once

#include "mesh.hpp"

class Box {
private:
	Material* mat;
public:
	std::vector<Mesh*> meshes;
public:
	Box();
	~Box();
};