#pragma once

#include "mesh.hpp"

class Cube : public Mesh {
public:
	Cube(uint id);
	~Cube();
};